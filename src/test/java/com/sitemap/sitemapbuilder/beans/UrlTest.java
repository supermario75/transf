package com.sitemap.sitemapbuilder.beans;

import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;

public class UrlTest
{
  private DateFormat w3dtfFormat = new SimpleDateFormat(
          "yyyy-MM-dd'T'HH:mm:ss'Z'");


  @Test
  public void standardUrl()
  {
    Url url = new SitemapUrlBuilder("www.vestititrendy.com")
            .withChangefreq(Url.ChangeFreq.MONTHLY)
            .withPriority(Url.Priority.MAX)
            .withtLastMod(new Date())
            .build();

    String expected ="\n  <url>\n" +
                     "\t<loc>www.vestititrendy.com</loc>\n" +
                     "\t<lastmod>"+ w3dtfFormat.format(url.getLastMod()) + "</lastmod>\n" +
                     "\t<changefreq>monthly</changefreq>\n" +
                     "\t<priority>1.0</priority>\n" +
                     "  </url>";

    Assert.assertThat(url.toString(),is(expected));


  }

  @Test
  public void urlWithImages()
  {
    Url url = new SitemapUrlBuilder("www.vestititrendy.com")
            .withChangefreq(Url.ChangeFreq.MONTHLY)
            .withPriority(Url.Priority.MAX)
            .withtLastMod(new Date())
            .withUrlImages(Arrays.asList("www.url1.com/img1.gif","www.url1.com/img2.gif"))
            .build();

    String expected ="\n  <url>\n" +
                     "\t<loc>www.vestititrendy.com</loc>\n" +
                     "\t<lastmod>"+ w3dtfFormat.format(url.getLastMod()) + "</lastmod>\n" +
                     "\t<changefreq>monthly</changefreq>\n" +
                     "\t<priority>1.0</priority>\n" +
                     "<image:image>\n" +
                     "\t<image:loc>www.url1.com/img1.gif</image:loc>\n" +
                     "</image:image>\n" +
                     "<image:image>\n" +
                     "\t<image:loc>www.url1.com/img2.gif</image:loc>\n" +
                     "</image:image>" +
                     "\n  </url>";

    Assert.assertThat(url.toString(),is(expected));


  }
}
