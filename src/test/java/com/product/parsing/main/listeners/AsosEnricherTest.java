package com.product.parsing.main.listeners;

import com.product.parsing.Product;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

public class AsosEnricherTest {

  @Test
  public void createAdditionalImagesFrom_firstAdditionalImage()
  {

      Product product = new Product();
      product.getAdditionalImages().add("https://images.asos-media.com/inv/media/8/9/9/5/1975998/black/image1s.jpg");
      new AsosEnricher().enrichFrom(product);

      Assert.assertThat(product.getAdditionalImages(), Matchers.hasItem("https://images.asos-media.com/inv/media/8/9/9/5/1975998/image2xxl.jpg"));
      Assert.assertThat(product.getAdditionalImages(), Matchers.hasItem("https://images.asos-media.com/inv/media/8/9/9/5/1975998/image3xxl.jpg"));
      Assert.assertThat(product.getAdditionalImages(), Matchers.hasItem("https://images.asos-media.com/inv/media/8/9/9/5/1975998/image1xxl.jpg"));
  }

  @Test
  public void createAdditionalImagesFrom_largeImage_withExtrachars()
  {

      Product product = new Product();
      product.getAdditionalImages().add("https://images.asos-media.com/inv/media/8/9/9/5/1975998/black/image1s_sdsdsdfsd.jpg");
      new AsosEnricher().enrichFrom(product);

      Assert.assertThat(product.getAdditionalImages(), Matchers.hasItem("https://images.asos-media.com/inv/media/8/9/9/5/1975998/image2xxl.jpg"));
      Assert.assertThat(product.getAdditionalImages(), Matchers.hasItem("https://images.asos-media.com/inv/media/8/9/9/5/1975998/image3xxl.jpg"));
      Assert.assertThat(product.getAdditionalImages(), Matchers.hasItem("https://images.asos-media.com/inv/media/8/9/9/5/1975998/image1xxl.jpg"));
  }

}