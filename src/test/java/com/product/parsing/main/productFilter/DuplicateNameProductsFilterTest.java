package com.product.parsing.main.productFilter;

import com.product.parsing.Product;
import com.product.parsing.Products;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class DuplicateNameProductsFilterTest
{

  @Test
  public void test()
  {
    Products products = new Products();
    products.add(newProduct("ciccio bello"));
    products.add(newProduct("ciccio Bello"));
    products.add(newProduct("ciccio Bello"));
    products.add(newProduct("ciccio Bello"));
    products.add(newProduct("pluto"));

    DuplicateNameProductsFilter duplicateNameProductsFilter = new DuplicateNameProductsFilter();

    Products filteredProducts = duplicateNameProductsFilter.filter(products);

    Assert.assertThat(filteredProducts.size(),is(2));

    Assert.assertThat("different first prod name: ",filteredProducts.get(0).getName(),is("ciccio bello"));
    Assert.assertThat("different second prod name: ",filteredProducts.get(1).getName(),is("pluto"));


  }

  private Product newProduct(String name)
  {
    Product e = new Product();
    e.setName(name);
    return e;
  }
}
