package com.product.parsing.main.beans;

import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.writer.ProductsReader;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.is;

public class QualityGateProductsReaderTest {


    @Test
    public void readAndFilterProducts() throws Exception {

        ProductsReader delegate = (inputFileName, category) -> {
            Products products = new Products();
            products.add(product("a looooooooooooog name1", "a short Description", "100"));
            products.add(product("a looooooooooooog name2", "a looooooooooooong  Description", "100"));
            return products;
        };
        Products products = new QualityGateProductsReader(delegate).readAndFilterProducts("aaa", null);

        Assert.assertThat(products.size(),is(1));
        Assert.assertThat(products.get(0).getName(),is("a looooooooooooog name2"));
    }

    private Product product(String name, String description, String price) {
        Product product = new Product();
        product.setDescription(description);
        product.setName(name);
        product.setPrice(price);
        return product;
    }

}