package com.product.parsing.main.beans.category;

import com.flexiblecatalog.site.common.Cobrand;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

public class CategoriesTest
{

    @Test
    public void sameInstance()
    {
      Categories instance1 = Categories.getInstance(Cobrand.VESTITI_TRENDY);
      Categories instance2 = Categories.getInstance(Cobrand.VESTITI_TRENDY);

      assertThat(instance1 == instance2,is(true));
    }

  @Test
  public void differentInstance()
  {
    Categories instance1 = Categories.getInstance(Cobrand.VESTITI_TRENDY);

    Categories instance2 = Categories.getInstance(Cobrand.VETEMENT_DE_TENDANCE);

    assertThat(instance1 == instance2,is(false));
  }


   @Test
   public void categories_vestitiTrendy()
   {
     final Categories categories = Categories.getInstance(Cobrand.VESTITI_TRENDY);

     assertThat(categories.size(), is(greaterThan(0)));

     final List<Category> onlyWomancategories = categories.onlyWomancategories();

     assertThat(onlyWomancategories.size(), is(4));

   }

  @Test
  public void categories_vetements()
  {
    final Categories categories = Categories.getInstance(Cobrand.VETEMENT_DE_TENDANCE);

    assertThat(categories.size(), is(greaterThan(0)));


  }
    @Test
    public void shouldSortField()
    {
        final Categories categories = Categories.getInstance(Cobrand.VETEMENT_DE_TENDANCE);

        Optional<Category> manteauxCategory = categories.findByFriendlyName("manteaux-et-vestes");
        Optional<Category> duffelCoatCategory = categories.findByFriendlyName("duffel-coat");


        assertThat(manteauxCategory.isPresent(), is(true));
        assertThat("wrong manteauxCategory ShouldSort",manteauxCategory.get().isShouldSort(), is(false));

        assertThat(duffelCoatCategory.isPresent(), is(true));
        assertThat("wrong duffelCoat ShouldSort",duffelCoatCategory.get().isShouldSort(), is(false));


    }


}