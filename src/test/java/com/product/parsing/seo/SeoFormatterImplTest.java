package com.product.parsing.seo;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SeoFormatterImplTest
{
  SeoFormatterImpl underTest = new SeoFormatterImpl();

  @Test
  public void diacriticsCharacters_removed() throws Exception
  {
    assertThat(underTest.nameToSeoName("marrón"),
               is("marron"));
  }
  @Test
  public void simpleNameWithSpaces() throws Exception
  {
    assertThat(underTest.nameToSeoName("FICHISSIMA BIKINI BIKINI NERO"),
                                    is("fichissima-bikini-bikini-nero"));
  }

  @Test
  public void simpleNameWithMinus() throws Exception
  {
    assertThat(underTest.nameToSeoName("FICHISSIMA - BIKINI BIKINI NERO"),
                                    is("fichissima-bikini-bikini-nero"));
  }

  @Test
  public void simpleNameWithSpacesAndMinus() throws Exception
  {
    assertThat(underTest.nameToSeoName("FICHISSIMA TOP IMBOTTITO BIKINI BIANCO - NERO"),
                                    is("fichissima-top-imbottito-bikini-bianco-nero"));
  }

  @Test
  public void percentage() throws Exception
  {
    assertThat(underTest.nameToSeoName("asos confezione da 2 t shirt con scollo rotondo risparmia il 17% blu navy_nero"),
            is("asos-confezione-da-2-t-shirt-con-scollo-rotondo-risparmia-il-17-percento-blu-navy_nero"));
  }

  @Test
  public void slashFormattedToUnderscore() throws Exception
  {
    assertThat(underTest.nameToSeoName("Marc By Marc Jacobs MMJ 096/N/S Occhiali da sole bianco"),
               is("marc-by-marc-jacobs-mmj-096_n_s-occhiali-da-sole-bianco"));
  }


  @Test
  public void ampFormatted() throws Exception
  {
    assertThat(underTest.nameToSeoName("FRANKLIN &amp; MARSHALL T SHIRT MAN T-SHIRT MANICA CORTA ARANCIONE"),
               is("franklin-and-marshall-t-shirt-man-t-shirt-manica-corta-arancione"));
  }

  @Test
  public void andFormatted() throws Exception
  {
    assertThat(underTest.nameToSeoName("FRANKLIN&MARSHALL T SHIRT MAN T-SHIRT MANICA CORTA ARANCIONE"),
               is("franklin-and-marshall-t-shirt-man-t-shirt-manica-corta-arancione"));
  }
/*  @Test
  public void specialCharactersEncoded() throws Exception
  {
    assertThat(underTest.nameToSeoName("Sandali Les Tropéziennes par M Belarbi HERLUNE"),
               is("sandali-les-trop%C3%A9ziennes-par-m-belarbi-herlune"));
  }*/

  @Test
  public void dotFormattedToUnderscore() throws Exception
  {
    assertThat(underTest.nameToSeoName("Scarpe C.Doux CLOE"),
               is("scarpe-c_doux-cloe"));
  }


}
