package com.product.parsing;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.is;


public class InvalidCharsRemoverTest {

    @Test
    public void removeChar()
    {
       String name = "Columbia - Challenger -<giacca con cappuccio isolante verde/nero - Verde";

        String result = InvalidCharsRemover.removeFrom(name);
        Assert.assertThat(result,is("Columbia - Challenger -giacca con cappuccio isolante verde/nero - Verde"));
    }
}
