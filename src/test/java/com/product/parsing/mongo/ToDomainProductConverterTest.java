package com.product.parsing.mongo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import com.flexiblecatalog.domain.Product;
import com.product.parsing.Products;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class ToDomainProductConverterTest
{



  private com.product.parsing.Product product;
  private Product firstConvertedProduct;

  @Test
  public void name()
  {
    givenProduct();
    product.setName("pippo");
    whenConvert();
    assertThat(firstConvertedProduct.getName(), is("pippo"));
  }

  @Test
  public void seoName()
  {
    givenProduct();
    product.setSeoName("pippo-baudo");
    whenConvert();
    assertThat(firstConvertedProduct.getSeoName(), is("pippo-baudo"));
  }

  @Test
  public void manufacturer()
  {
    givenProduct();
    product.setManufacturer("manufacturer uno");
    whenConvert();
    assertThat(firstConvertedProduct.getManufacturer(), is("manufacturer uno"));
    assertThat(firstConvertedProduct.getSeoBrand(),is("manufacturer-uno"));
  }

  @Test
  public void url()
  {
    givenProduct();
    product.setUrl("url");
    whenConvert();
    assertThat(firstConvertedProduct.getSaleUrl(), is("url"));
  }

  @Test
  public void color()
  {
    givenProduct();
    product.setColor("color");
    whenConvert();
    assertThat(firstConvertedProduct.getColor(), is("color"));
  }

  @Test
  public void deltime()
  {
    givenProduct();
    product.setDeliveryTime("deltime");
    whenConvert();
    assertThat(firstConvertedProduct.getDeliveryTime(), is("deltime"));
  }

  @Test
  public void description()
  {
    givenProduct();
    product.setDescription("description");
    whenConvert();
    assertThat(firstConvertedProduct.getDescription(), is("description"));
  }

  @Test
  public void imageUrl()
  {
    givenProduct();
    product.setImageUrl("imageUrl");
    whenConvert();
    assertThat(firstConvertedProduct.getImageUrl(), is("imageUrl"));
  }

  @Test
  public void largeimageurl()
  {
    givenProduct();
    product.setLargeImageUrl("largeimageurl");
    whenConvert();
    assertThat(firstConvertedProduct.getLargeImageUrl(), is("largeimageurl"));
  }

  @Test
  public void oldPrice()
  {
    givenProduct();
    product.setOldPrice("34.56");
    whenConvert();
    assertThat(firstConvertedProduct.getOldPrice(), is(new BigDecimal("34.56")));
  }

  @Test
  public void samePriceAndOldPrice()
  {
    givenProduct();
    product.setPrice("34.56");
    product.setOldPrice("34.56");
    whenConvert();
    assertThat(firstConvertedProduct.getOldPrice(), is(nullValue()));
  }

  @Test
  public void price()
  {
    givenProduct();
    product.setPrice("456.7");
    whenConvert();
    assertThat(firstConvertedProduct.getPrice(), is(new BigDecimal("456.7")));
  }

  @Test
  public void seocolor()
  {
    givenProduct();
    product.setSeoColor("seocolor");
    whenConvert();
    assertThat(firstConvertedProduct.getSeoColor(), is("seocolor"));
  }

  @Test
  public void seoname()
  {
    givenProduct();
    product.setSeoName("seoname");
    whenConvert();
    assertThat(firstConvertedProduct.getSeoName(), is("seoname"));
  }

  @Test
  public void shipcost()
  {
    givenProduct();
    product.setShippingHandlingCost("shipcost");
    whenConvert();
    assertThat(firstConvertedProduct.getShippingHandlingCost(), is("shipcost"));
  }

  @Test
  public void category()
  {
    givenProduct();
    product.setCategory("mycategory");
    whenConvert();
    assertThat(firstConvertedProduct.getCategory(), is("mycategory"));
  }

  @Test
  public void tags()
  {
    givenProduct();
    product.addTag("ciccio");
    whenConvert();
    assertThat(firstConvertedProduct.getTagList().get(0).getName(), is("ciccio"));
  }


  @Test
  public void gender()
  {
    givenProduct();
    product.setGender("uomo");
    whenConvert();
    assertThat(firstConvertedProduct.getGender(), is("uomo"));
  }

  @Test
  public void supplier()
  {
    givenProduct();
    product.setSupplier("celio");
    whenConvert();
    assertThat(firstConvertedProduct.getSupplier(), is("celio"));
  }


  private void whenConvert()
  {
    Products products = new Products();
    products.add(product);
    final List<Product> convertedProducts = ToDomainProductConverter.convert(products);
    firstConvertedProduct = convertedProducts.get(0);
  }

  private void givenProduct()
  {
    product = newProduct();
  }

  private com.product.parsing.Product newProduct()
  {
    return new com.product.parsing.Product();
  }
}
