package com.product.parsing;

import com.product.parsing.seo.SeoFormatter;
import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class ProductTest
{
  @Rule
  public JUnitRuleMockery context = new JUnitRuleMockery();
  SeoFormatter seoFormatter;
  Product underTest;

  @Before
  public void setUp() throws Exception
  {
    seoFormatter = context.mock(SeoFormatter.class);
    underTest = new Product(seoFormatter);
  }

  @Test
  public void whenSetNameCalculateSeoName()
  {
    context.checking(new Expectations() {{
      oneOf(seoFormatter).nameToSeoName("ciccio bello");
    }});

    underTest.setIfNotPresentName("ciccio bello");
  }
}
