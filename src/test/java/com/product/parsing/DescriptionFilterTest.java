package com.product.parsing;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DescriptionFilterTest
{
  @Test
  public void filter() throws Exception
  {
    DescriptionFilter underTest = new DescriptionFilter();

    String filter = underTest.filter("start su giu su Zalando IT end");

    assertThat(filter, is("start su giu  end"));
  }
}
