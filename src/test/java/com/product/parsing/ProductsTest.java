package com.product.parsing;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static com.product.parsing.ProductsTest.ProductBuilder.product;
import static org.hamcrest.core.Is.is;

public class ProductsTest {


    @Test
    public void sortme() throws Exception {

        Products products = new Products();


        products.add(product()
                .withName("ciao")
                .withDescription("a super super super long sample Description")
                .build());
        products.add(product()
                .withName("VERY VERY LONG NAME")
                .withDescription("a sample Description")
                .build());
        products.add(product()
                .withName("MEDIUM name")
                .withDescription("a sample Description")
                .build());
        products.add(product()
                .withName("AA")
                .withDescription("a sample Description")
                .build());


        products.sortByQuality();

        Assert.assertThat(products.getFirst().getDescription(),is("a super super super long sample Description"));
        Assert.assertThat(products.getFirst().getName(),is("ciao"));
    }


    @Test
    public void sortWithNullDescription() throws Exception {

        Products products = new Products();


        products.add(product()
                .withName("ciao")
                .withDescription(null)
                .build());
        products.add(product()
                .withName("VERY VERY LONG NAME")
                .withDescription("a sample Description")
                .build());



        products.sortByQuality();

        Assert.assertThat(products.getFirst().getDescription(),is("a sample Description"));
        Assert.assertThat(products.getFirst().getName(),is("VERY VERY LONG NAME"));
    }

    public static class ProductBuilder {
        private String name ="default name";
        private String description = "default description";

        public static ProductBuilder product()
        {
          return new ProductBuilder();
        }


        public Product build() {
            Product p1 = new Product();
            p1.setName(name);
            p1.setDescription(description);
            return p1;
        }

        public ProductBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ProductBuilder withDescription(String description) {
            this.description = description;
            return this;
        }
    }
}