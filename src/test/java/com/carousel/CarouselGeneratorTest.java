package com.carousel;

import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.taggedProducts.TaggedProduct;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class CarouselGeneratorTest
{
  CarouselGenerator underTest;
//
//  app.controller('CarouselWomanCtrl',function ($scope) {
//  $scope.myInterval = 5000;
//  var slides = $scope.slides = [];
//  $scope.addSlide = function() {
//    var newWidth = slides.length;
//    slides.push({
//                        image: '/carousel/slide_woman_' + newWidth + '.jpg',
//            text: ['Fichissima Bikini',
//            'Sandali C.Doux DUBAI ECA',
//            'Kisskin Fascia Caramella',
//            'Infradito Lotus SICILY'][slides.length % 4] ,
//    url:  ['Fichissima',
//            'Sandali C.Doux',
//            'Kisskin Fascia',
//            'Infradito Lotus'][slides.length % 4]
//    });
//  };
//  for (var i=0; i<4; i++) {
//    $scope.addSlide();
//  }
//});



  @Test
  public void createCarousel() throws Exception
  {

    Products products = new Products();
    products.add(newWomanProduct("Fichissima Bikini", "http://www.spartoo.com", "http://FichissimaImage.jpg",
                                 "40.5 Euro"));
    products.add(newWomanProduct("Sandali C.Doux DUBAI ECA", "Sandali C.Doux", "http://ECA.jpg", "100 Euro"));
    products.add(newWomanProduct("Kisskin Fascia Caramella", "Kisskin Fascia", "http://Kisskin.jpg", "23.67 Euro"));
    products.add(newManProduct("Infradito Lotus SICILY", "Infradito Lotus", "http://Lotus.jpg", "89 Euro"));

    underTest = new CarouselGenerator();

    long currentTimeMillis = System.currentTimeMillis();

    String basePath = "dummyCobrand";
    String language ="fr";
    String womanCarousel = underTest.createCarousel("Woman", basePath, products,currentTimeMillis, language);
    String manCarousel = underTest.createCarousel("Man", basePath, products,currentTimeMillis, language);


    assertThat(womanCarousel, containsString("var version = " + currentTimeMillis + ";"));
    assertThat(womanCarousel, containsString("image: '/carousel/" + basePath + "/slide_woman_' + newWidth + '.jpg?version=' + version,"));


    assertThat(womanCarousel, containsString("app.controller('CarouselWomanCtrl'"));
    assertThat(womanCarousel, containsString("image: '/carousel/" + basePath + "/slide_woman_'"));

    assertThat(womanCarousel, containsString("text: ["));
    assertThat(womanCarousel, containsString("'Fichissima Bikini',"));

    assertThat(womanCarousel, containsString("url: ["));
    assertThat(womanCarousel, containsString("'femme/dummytag/detail/Fichissima Bikini',"));

    assertThat(womanCarousel, containsString("price: ["));
    assertThat(womanCarousel, containsString("40.5 Euro"));

    assertThat(womanCarousel, containsString("[slides.length % " + products.size() + "]"));
    assertThat(womanCarousel, containsString("    for (var i=0; i<" + products.size() + "; i++) {\n"));


//    assertThat(womanCarousel, containsString("wget -O /opt/stark-basin-3452/src/main/webapp/carousel/slide_woman_0.jpg http://FichissimaImage.jpg"));

    System.out.println(womanCarousel);
    System.out.println(manCarousel);



  }

  private Product newWomanProduct(String name, String url, String imageUrl, String price)
  {
    return newProcuct(name, url, imageUrl, price, "donna");
  }

  private Product newManProduct(String name, String url, String imageUrl, String price)
  {
    return newProcuct(name, url, imageUrl, price, "uomo");
  }

  private TaggedProduct newProcuct(String name, String url, String imageUrl, String price, String gender)
  {
    Product product = new Product();
    product.setName(name);
    product.setUrl(url);
    product.setLargeImageUrl(imageUrl);
    product.setPrice(price);
    product.setSeoName(name);

    return new TaggedProduct(product, "dummytag", gender);
  }

}
