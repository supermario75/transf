package com.product.parsing;

import java.util.Arrays;
import java.util.List;

public enum ProductEvent
{
  START_PRODUCT("product"),
  URL("deepLink"),
  GENDER("gender"),
  SMALL_IMAGE_URL("smallImage"),
  MEDIUM_IMAGE_URL("mediumImage"),
  LARGE_IMAGE_URL("largeImage"),
  DESCRIPTION("description"),
  LONG_DESCRIPTION("longDescription"),
  SHIPPING_HANDLING_COST("shippingHandlingCosts"),
  DELIVERY_TIME("deliveryTime"),
  MANUFACTURER("manufacturer"),
  NAME("name"),
  NAME_PREVIOUS("namePrevious"),
  PRICE("price"),
  OLD_PRICE("oldPrice"),
  TAGS("merchantCategory","merchantCategoryPath","merchantMainCategory","merchantProductSecondCategory","merchantSubCategory","merchantThirdCategory"),
  ADDITIONAL_IMAGES(
                    "additionalImage1",
                    "additionalImage2",
                    "additionalImage3",
                    "additionalImage4",
                    "additionalImage5",
                    "additionalImage6",
                    "additionalImage7",
                    "additionalImage8",
                    "additionalImage9",
                    "additionalImage10",
                    "additionalImage11",
                    "additionalImage12"
  ),
  EXTRA1("extra1"),
  EXTRA2("extra2"),
  COLOR("color"),
  SIZE("size"),
  MATERIAL("material"),
  ADDITIONAL_FEATURES("additionalFeatures"),
  ADDITIONAL_FEATURES_MAP("additionalFeaturesMap"),
  PIXEL("pixel"),
  SKU("sku"),
  END_PRODUCT("product");

  private List<String> tags;

  ProductEvent(String ... tags)
  {
    this.tags = Arrays.asList(tags);
  }

  public boolean isTag(String content)
  {
    return tags.contains(content);
  }

  public void evaluateAndFire(String tagname,String value, ProductEventListener listener)
  {
    if (isTag(tagname))
       listener.fireEvent(this,value);
  }

}
