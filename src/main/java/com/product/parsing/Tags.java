package com.product.parsing;

import java.util.LinkedHashSet;

class Tags extends LinkedHashSet<String>
{

  @Override
  public boolean add(String input)
  {
    if (input != null)
    {
      String[] tags = input.split("/");
      for (String tag : tags)
      {
        super.add(tag.toLowerCase());
      }
      return true;

    }
    return false;
  }
}
