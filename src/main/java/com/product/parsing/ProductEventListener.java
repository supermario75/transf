package com.product.parsing;

public interface ProductEventListener
{
  void fireEvent(ProductEvent event, String content);


}
