package com.product.parsing;

import com.flexiblecatalog.site.common.Gender;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Comparator.comparing;
import static org.apache.commons.lang3.Validate.notEmpty;

public class Products extends LinkedList<Product>
{
  public static Logger logger = Logger.getLogger(Products.class);

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("[\n");
    Iterator<Product> iterator = this.iterator();
    while (iterator.hasNext())
    {
      Product product = iterator.next();

      sb.append("\n").append(product.toString());
      if (iterator.hasNext())
        sb.append(",");
    }

    sb.append("\n]");
    return sb.toString();
  }

  public Products filerByTag(String uomo)
  {
    Products filtered = new Products();
    for (Product product : this)
    {
      if (product.hasTag(uomo))
        filtered.add(product);
    }

    return filtered;
  }

  public void setGender(String gender)
  {
    for (Product product : this)
    {
      product.setGender(gender);
    }
  }

  public Map<Gender, Products> divideByGender()
  {
    Map<Gender,Products> genderMap = new HashMap<>();
    genderMap.put(Gender.WOMAN,new Products());
    genderMap.put(Gender.MAN,new Products());
    genderMap.put(Gender.CHILDREN,new Products());

    for (Product product : this)
    {
      if (!product.hasAnyGender())
        product.setGenderFromTagOrDefault(Gender.WOMAN);

      Gender gender = Gender.valueOf(product.getGender());
      Products products = genderMap.get(gender);
      products.add(product);
    }
    return genderMap;

  }

  private void resolveByTags(Map<Gender,Products> genderMap, Product product)
  {
    final List<String> womanTags = Arrays.asList("donna", "woman", "femme", "mujer");
    final List<String> childrenTag = Arrays.asList("Bambino", "bimbo" ,"bimba");

    product.setGenderFromTagOrDefault(Gender.WOMAN);
    if (product.hasAnyTags(womanTags))
    {
      product.setGender(Gender.WOMAN.name());
      genderMap.get(Gender.WOMAN).add(product);
    }
    if (product.hasAnyTags(childrenTag))
    {
      product.setGender(Gender.CHILDREN.name());
      genderMap.get(Gender.CHILDREN).add(product);
    }
    else
    {
      product.setGender(Gender.MAN.name());
      genderMap.get(Gender.MAN).add(product);
    }

  }

  public void sortByQuality() {
    Comparator<Product> qualityComparator =
            comparing((Product c) -> lengthOrZero(c.getAdditionalImages())).reversed()
                .thenComparing(comparing((Product c) -> lengthOrZero(c.getDescription())).reversed())
                .thenComparing(comparing((Product c) -> c.getName().length()).reversed());
    Collections.sort(this, qualityComparator);
  }

  private int lengthOrZero(Set<String> additionalImages) {
    return additionalImages != null ? additionalImages.size() : 0;
  }

  private int lengthOrZero(String aString) {
    return aString != null ? aString.length() : 0;
  }


}
