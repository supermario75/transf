package com.product.parsing.seo;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public class SeoFormatterImpl implements SeoFormatter
{
  private static SeoFormatterImpl instance = new SeoFormatterImpl();

  public static SeoFormatterImpl getInstance()
  {
    return instance;
  }

  @Override
  public String nameToSeoName(String name)
  {
    String result = name.toLowerCase();

    result = StringUtils.stripAccents(result);

    for (Rules rule : Rules.values())
      result = rule.replaceAll(result);

    return result;
  }

  enum Rules
  {
    MINUS_BETWEEN_SPACES(" - ","-"),
    SPACE(" ","-"),
    SLASH_TO_UNDERSCORE("/","_"),
    DOT_TO_UNDERSCORE("\\.","_"),
    AMP_REPLACER("&amp;","and"),
    AND_REPLACER("&","-and-"),
    PERCENTAGE_REPLACER("%","-percento")

    ;

    private final String from;
    private final String to;

    private Pattern pattern;

    Rules(String from, String to)
    {
      this.from = from;
      this.to = to;
      this.pattern = Pattern.compile(from);
    }

    public String replaceAll(String input)
    {
      return pattern.matcher(input).replaceAll(to);
    }
  }
}
