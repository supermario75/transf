package com.product.parsing.seo;

public interface SeoFormatter
{
  String nameToSeoName(String name);
}
