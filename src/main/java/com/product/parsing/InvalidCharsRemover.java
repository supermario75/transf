package com.product.parsing;

import java.util.Arrays;
import java.util.List;

public class InvalidCharsRemover
{
    private static List<String> invalidChars = Arrays.asList("<");
    public static String removeFrom(String tagContent)
    {
      return invalidChars.stream()
                         .map(c -> tagContent.replace(c,""))
                         .findFirst().get();
    }
}
