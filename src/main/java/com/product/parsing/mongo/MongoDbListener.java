package com.product.parsing.mongo;

import com.flexiblecatalog.domain.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.CategorizedProducts;
import com.product.parsing.main.events.Event;
import com.product.parsing.main.events.EventType;
import com.product.parsing.main.listeners.ProductGenerationEventListener;
import com.product.parsing.mongo.repository.JsonProductRepository;
import org.apache.log4j.Logger;

import java.util.List;

public class MongoDbListener implements ProductGenerationEventListener
{
  Logger logger = Logger.getLogger(MongoDbListener.class);

  JsonProductRepository repository;

  @Override
  public void fireEvent(Event event)
  {
    if (!event.getConf().isCreateDb())
      return;



    if (event.getEventType() == EventType.CATEGORIZED_PRODUCT_CREATED)
    {
      logger.debug(" ---------------------------> evento " + event.getEventType());
      repository = JsonProductRepositoryFactory.getInstance();

      final CategorizedProducts categorizedProducts = event.getProducts();
      final Products menCategorized = categorizedProducts.getMenCategorized();
      final Products womenCategorized = categorizedProducts.getWomenCategorized();
      final Products childrenCategorized = categorizedProducts.getChildrenCategorized();

      final String category = categorizedProducts.getCategory().getFriendlyNameLowercase();

      final Long num = repository.deleteByCategory(category);
      logger.debug("deleted all product with category " + category + " num " + num);

      List<Product> products = ToDomainProductConverter.convert(womenCategorized);
      repository.save(products);
      logger.debug("woman products " + products.size() + " saved");

      products = ToDomainProductConverter.convert(menCategorized);
      repository.save(products);
      logger.debug("men products " + products.size() + " saved");

      products = ToDomainProductConverter.convert(childrenCategorized);
      repository.save(products);
      logger.debug("children products " + products.size() + " saved");

    }


}


}
