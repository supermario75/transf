package com.product.parsing.mongo.repository;

import com.flexiblecatalog.domain.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JsonProductRepository extends MongoRepository<Product,String>
{
  List<Product> findByName(String name);
  List<Product> findByColor(String color);

  Long deleteByCategory(String color);
}
