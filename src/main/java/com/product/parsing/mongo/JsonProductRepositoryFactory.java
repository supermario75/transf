package com.product.parsing.mongo;

import com.product.parsing.mongo.repository.JsonProductRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JsonProductRepositoryFactory
{
  private static JsonProductRepository repository;

  public static JsonProductRepository getInstance()
  {
    if (repository == null)
      repository = createRepositoryFromContext();

    return repository;
  }

  private static JsonProductRepository createRepositoryFromContext()
  {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("mongodb-context.xml");
    return (JsonProductRepository) applicationContext.getBean("jsonProductRepository");
  }



}
