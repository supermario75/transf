package com.product.parsing.mongo;

import com.flexiblecatalog.domain.Product;
import com.flexiblecatalog.domain.Tag;
import com.product.parsing.Products;
import com.product.parsing.seo.SeoFormatter;
import com.product.parsing.seo.SeoFormatterImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ToDomainProductConverter
{
  private static SeoFormatter seoFormatter = SeoFormatterImpl.getInstance();
  public static List<Product> convert(Products menCategorized)
  {
    List<Product> outputList = new ArrayList<>();
    for (com.product.parsing.Product product : menCategorized)
    {
      Product jSonProduct = convertProduct(product);
      outputList.add(jSonProduct);
    }
    return outputList;
  }

  private static Product convertProduct(com.product.parsing.Product product)
  {
    Product convertedProduct = new Product();
    convertedProduct.setName(product.getName());
    convertedProduct.setManufacturer(product.getManufacturer());
    convertedProduct.setSaleUrl(product.getUrl());
    convertedProduct.setColor(product.getColor());
    convertedProduct.setDeliveryTime(product.getDeliveryTime());
    convertedProduct.setDescription(product.getDescription());
    convertedProduct.setImageUrl(product.getImageUrl());
    convertedProduct.setLargeImageUrl(product.getLargeImageUrl());
    convertOldPrice(product, convertedProduct, product.getPrice(), product.getOldPrice());
    convertedProduct.setPrice(toBigDecimalOrNull(product.getPrice()));
    convertedProduct.setSeoColor(product.getSeoColor());
    convertedProduct.setSeoName(product.getSeoName());
    convertedProduct.setShippingHandlingCost(product.getShippingHandlingCost());
    convertedProduct.setTagList(convertTagList(product.getTags()));
    convertedProduct.setSeoBrand(product.getSeoManufacturer());
    convertedProduct.setCategory(product.getCategory());
    convertedProduct.setGender(product.getGender());
    convertedProduct.setSupplier(product.getSupplier());

    return convertedProduct;
  }

  private static void convertOldPrice(com.product.parsing.Product product, Product convertedProduct, String price, String oldPrice) {
    if (price.equals(oldPrice))
      convertedProduct.setOldPrice(null);
    else
      convertedProduct.setOldPrice(toBigDecimalOrNull(product.getOldPrice()));
  }

  private static List<Tag> convertTagList(Set<String> tags)
  {
    List<Tag> tagList = new ArrayList<>();
    for (String tag : tags)
    {
      tagList.add(new Tag(tag));
    }
    return tagList;
  }

  private static BigDecimal toBigDecimalOrNull(String v)
  {
    return isNotEmpty(v) ? new BigDecimal(v) : null;
  }

  private static boolean isNotEmpty(String v)
  {
    return v != null && !"".equals(v);
  }
}
