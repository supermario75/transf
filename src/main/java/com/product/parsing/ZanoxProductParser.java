package com.product.parsing;

import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static com.product.parsing.ProductEvent.START_PRODUCT;

public class ZanoxProductParser
{
  Logger logger = Logger.getLogger(ZanoxProductParser.class);
  private ProductEventListener listener;

  public ZanoxProductParser(ProductEventListener listener)
  {
    this.listener = listener;
  }

 public void parseProducts(String filename)
  {
    try
    {
      logger.info("tring to parse file " + filename );
      tryParseProducts(filename);
    }
    catch (FileNotFoundException fne)
    {
      logger.error("error file not found " + filename );
    }
    catch (Exception e)
    {
      logger.error("error parsing file " + filename + " " + e.getMessage(), e);
    }
  }

  private void tryParseProducts(String fileName) throws XMLStreamException, FileNotFoundException
  {
    String tagContent = "";
    XMLInputFactory factory = XMLInputFactory.newInstance();

    XMLStreamReader reader = factory.createXMLStreamReader(resolveInputStream(fileName));

    while (reader.hasNext())
    {
      int event = reader.next();

      switch (event)
      {
        case XMLStreamConstants.START_ELEMENT:

          START_PRODUCT.evaluateAndFire(reader.getLocalName(),"",listener);
          break;

        case XMLStreamConstants.CHARACTERS:
          tagContent = tagContent + reader.getText().trim();
          break;

        case XMLStreamConstants.END_ELEMENT:
          String localName = reader.getLocalName();

          for (ProductEvent productEvent : ProductEvent.values())
          {
            if (productEvent != START_PRODUCT)
            {
              productEvent.evaluateAndFire(localName,tagContent,listener);
            }
          }

          tagContent="";
          break;

        case XMLStreamConstants.START_DOCUMENT:
          break;
      }

    }

  }

  private InputStream resolveInputStream(String fileName) throws FileNotFoundException
  {
    return fileName.startsWith("/") ? new FileInputStream(fileName)
                                    : ClassLoader.getSystemResourceAsStream(fileName);

  }


}
