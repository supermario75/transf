package com.product.parsing.taggedProducts;

import com.product.parsing.Product;

import java.util.List;
import java.util.Set;

public class TaggedProduct extends Product
{
  private Product product;
  private String tag;
  private String gender;

  public TaggedProduct(Product product, String tag,String gender)
  {
    this.product = product;
    this.tag = tag;
    this.gender = gender;
  }

  public String getTag()
  {
    return tag;
  }

  public String getGender()
  {
    return gender;
  }

  @Override
  public String getImageUrl()
  {
    return product.getImageUrl();
  }

  @Override public void setImageUrl(String imageUrl)
  {
    product.setImageUrl(imageUrl);
  }

  @Override public String getUrl()
  {
    return product.getUrl();
  }

  @Override public void setUrl(String url)
  {
    product.setUrl(url);
  }

  @Override public String getName()
  {
    return product.getName();
  }

  @Override public void setName(String name)
  {
    product.setName(name);
  }

  @Override public String getNamePrevious()
  {
    return product.getNamePrevious();
  }

  @Override public void setNamePrevious(String name)
  {
    product.setNamePrevious(name);
  }

  @Override public String getPrice()
  {
    return product.getPrice();
  }

  @Override public void setPrice(String price)
  {
    product.setPrice(price);
  }

  @Override public String getManufacturer()
  {
    return product.getManufacturer();
  }

  @Override public void setManufacturer(String manufacturer)
  {
    product.setManufacturer(manufacturer);
  }

  @Override public String getLargeImageUrl()
  {
    return product.getLargeImageUrl();
  }

  @Override public void setLargeImageUrl(String largeImageUrl)
  {
    product.setLargeImageUrl(largeImageUrl);
  }

  @Override public Set<String> getTags()
  {
    return product.getTags();
  }

  @Override public String toString()
  {
    return product.toString();
  }

  @Override public String formatField(String name, String inputValue)
  {
    return product.formatField(name, inputValue);
  }

  @Override public String formatNumericField(String name, String value)
  {
    return product.formatNumericField(name, value);
  }

  @Override public String formatTags()
  {
    return product.formatTags();
  }

  @Override public void setIfNotPresentImageUrl(String tagContent)
  {
    product.setIfNotPresentImageUrl(tagContent);
  }

  @Override public void setIfNotPresentPrice(String tagContent)
  {
    product.setIfNotPresentPrice(tagContent);
  }

  @Override public void setIfNotPresentOldPrice(String tagContent)
  {
    product.setIfNotPresentOldPrice(tagContent);
  }

  @Override public void setIfNotPresentUrl(String tagContent)
  {
    product.setIfNotPresentUrl(tagContent);
  }

  @Override public void setIfNotPresentName(String tagContent)
  {
    product.setIfNotPresentName(tagContent);
  }

  @Override public void addTag(String tag)
  {
    product.addTag(tag);
  }

  @Override public String getOldPrice()
  {
    return product.getOldPrice();
  }

  @Override public void setOldPrice(String oldPrice)
  {
    product.setOldPrice(oldPrice);
  }

  @Override public void setIfNotManufacturer(String content)
  {
    product.setIfNotManufacturer(content);
  }

  @Override public void setIfNotPresentLargeImageUrl(String content)
  {
    product.setIfNotPresentLargeImageUrl(content);
  }

  @Override public void setIfNotPresentDescription(String content)
  {
    product.setIfNotPresentDescription(content);
  }

  @Override public void setIfNotPresentShippingHandlingCost(String content)
  {
    product.setIfNotPresentShippingHandlingCost(content);
  }

  @Override public void setIfNotPresentDeliveryTime(String content)
  {
    product.setIfNotPresentDeliveryTime(content);
  }

  @Override public boolean hasTag(String tag)
  {
    return product.hasTag(tag);
  }

  @Override public boolean hasAnyTags(List<String> specialTags)
  {
    return product.hasAnyTags(specialTags);
  }

  @Override public void setSeoName(String seoName)
  {
    product.setSeoName(seoName);
  }

  @Override public String getSeoName()
  {
    return product.getSeoName();
  }

  @Override public void setDescription(String description)
  {
    product.setDescription(description);
  }

  @Override public String getDescription()
  {
    return product.getDescription();
  }

  @Override public void setShippingHandlingCost(String shippingHandlingCost)
  {
    product.setShippingHandlingCost(shippingHandlingCost);
  }

  @Override public String getShippingHandlingCost()
  {
    return product.getShippingHandlingCost();
  }

  @Override public void setDeliveryTime(String deliveryTime)
  {
    product.setDeliveryTime(deliveryTime);
  }

  @Override public String getDeliveryTime()
  {
    return product.getDeliveryTime();
  }
}
