package com.product.parsing;

import org.apache.commons.lang3.StringUtils;

public class SquareBraceReplacer
{
  private static SquareBraceReplacer instance = new SquareBraceReplacer();

  public static SquareBraceReplacer getInstance()
  {
    return instance;
  }

  private SquareBraceReplacer() {}

  public String replaceBraces(String saleUrl)
  {
    String replace = StringUtils.replace(saleUrl, "[", "%5B");
    return StringUtils.replace(replace, "]", "%5D");
  }

}
