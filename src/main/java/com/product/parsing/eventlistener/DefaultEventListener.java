package com.product.parsing.eventlistener;

import com.flexiblecatalog.site.common.Gender;
import com.product.parsing.*;
import com.product.parsing.eventlistener.singleproduct.SingleProductListener;
import com.product.parsing.main.listeners.JsonFileWriterListener;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class DefaultEventListener implements ProductEventListener
{
  Logger logger = Logger.getLogger(JsonFileWriterListener.class);

  List<SingleProductListener> singleProductListeners;
  Pattern pattern;

  public DefaultEventListener(List<SingleProductListener> singleProductListeners)
  {
    this.singleProductListeners = singleProductListeners;

    pattern = Pattern.compile("\"");
  }

  private Product currentProduct = new Product();

  public List<String> tagsToExclude =  Arrays.asList("prodotto disponibile");

  @Override
  public void fireEvent(ProductEvent event, String content)
  {

    //    System.out.println("fired event " + event.name() + " with content " + content);

    switch (event)
    {
      case START_PRODUCT:
        currentProduct = new Product();
        break;

      case SMALL_IMAGE_URL:
        content = removeQuotes(content);
        currentProduct.setImageUrl(content);
        currentProduct.setIfNotPresentLargeImageUrl(content);
        break;

      case GENDER:
        content = removeQuotes(content);
        Gender gender = Gender.from(content);
        if (gender == null)
        {
          gender = Gender.WOMAN;
        }

        //  throw new RuntimeException("cannot find gender for content" + content);
        currentProduct.setGender(gender.name());
        break;

      case MEDIUM_IMAGE_URL:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentImageUrl(content);
        currentProduct.setIfNotPresentLargeImageUrl(content);
        break;

      case LARGE_IMAGE_URL:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentImageUrl(content);
        currentProduct.setLargeImageUrl(content);
        break;

      case PIXEL:
        content = removeQuotes(content);
        currentProduct.setPixel(content);
        break;

      case SKU:
        content = removeQuotes(content);
        currentProduct.setSku(content);
        break;

      case LONG_DESCRIPTION:
        content = removeQuotes(content);
        currentProduct.setDescription(content);
        break;

      case DESCRIPTION:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentDescription(DescriptionFilter.filter(content));
        break;

      case SHIPPING_HANDLING_COST:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentShippingHandlingCost(content);
        break;

      case DELIVERY_TIME:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentDeliveryTime(content);
        break;

      case PRICE:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentPrice(content);
        currentProduct.setIfNotPresentOldPrice("0");
        break;

      case OLD_PRICE:
        content = removeQuotes(content);
        currentProduct.setOldPrice(content);
        break;

      case URL:
        currentProduct.setIfNotPresentUrl(content);
        break;

      case NAME:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentName(content);
        break;

      case NAME_PREVIOUS:
        content = removeQuotes(content);
        currentProduct.setIfNotPresentNamePrevious(content);
        break;

      case MANUFACTURER:
        content = removeQuotes(content);
        currentProduct.setIfNotManufacturer(content);
        break;

      case COLOR:
        content = removeQuotes(content);
        currentProduct.setColor(content);
        break;

      case SIZE:
        content = removeQuotes(content);
        currentProduct.setSize(content);
        break;

      case ADDITIONAL_FEATURES:
        content = removeQuotes(content);
        currentProduct.setAdditionalFeatures(content);
        break;

      case ADDITIONAL_FEATURES_MAP:
        content = removeQuotes(content);
        currentProduct.setAdditionalFeaturesMap(content);
        break;

      case MATERIAL:
        content = removeQuotes(content);
        currentProduct.setMaterial(content);
        break;

      case TAGS:
        String[] tags = content.split("/");
        for (String tag : tags)
          formatAndAddTag(tag);
        break;

      case ADDITIONAL_IMAGES:
        content = removeQuotes(content);
        currentProduct.addAdditionalImage(content);
        break;

      case END_PRODUCT:
        singleProductListeners.stream()
                              .forEach(listener -> listener.onProduct(currentProduct));
        break;
    }
  }

  public void formatAndAddTag(String s)
  {
    String formattedTag = formatTag(s);
    if (!tagsToExclude.contains(formattedTag))
    {
      currentProduct.addTag(formattedTag);
    }
  }

  public Product getCurrentProduct()
  {
    return currentProduct;
  }

  private String formatTag(String s)
  {
    return removeQuotes(s.toLowerCase().trim());
  }

  private String removeQuotes(String content)
  {
    return pattern.matcher(content).replaceAll("");
  }




}
