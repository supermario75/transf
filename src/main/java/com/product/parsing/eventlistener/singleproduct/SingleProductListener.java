package com.product.parsing.eventlistener.singleproduct;

import com.product.parsing.Product;

public interface SingleProductListener
{
  public void onProduct(Product product);
}
