package com.product.parsing.main.events;

import com.product.parsing.main.beans.Supplier;
import com.product.parsing.main.beans.category.Category;

public class SupplierEvent extends Event
{
  Supplier supplier;
  Category category;

  public SupplierEvent(Supplier supplier, Category category, Event event)
  {
    super(event.eventType,event.getConf(),event.getProducts());
    this.supplier = supplier;
    this.category = category;
  }

  public Supplier getSupplier()
  {
    return supplier;
  }

  public Category getCategory()
  {
    return category;
  }

  public boolean isEventFor(Supplier supplier)
  {
      return this.supplier == supplier;
  }


}
