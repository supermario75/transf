package com.product.parsing.main.events;

import com.product.parsing.main.beans.CategorizedProducts;
import com.product.parsing.main.beans.ConfigurationParameter;

public class Event
{
  EventType eventType;

  CategorizedProducts products;
  ConfigurationParameter conf;


  public Event(EventType eventType, ConfigurationParameter conf)
  {
    this(eventType, conf, null);
  }

  public Event(EventType eventType, ConfigurationParameter conf, CategorizedProducts products)
  {
    this.eventType = eventType;
    this.conf = conf;
    this.products = products;
  }

  public EventType getEventType()
  {
    return eventType;
  }

  public CategorizedProducts getProducts()
  {
    return products;
  }

  public ConfigurationParameter getConf()
  {
    return conf;
  }

  public boolean hasType(EventType eventType) {
    return this.eventType == eventType;
  }
}
