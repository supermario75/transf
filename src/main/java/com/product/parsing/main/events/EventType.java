package com.product.parsing.main.events;

public enum EventType
{
  CATEGORIZED_PRODUCT_CREATED,
  SUPPLIER_PRODUCT_CREATION, PRODUCTS_GENERATION_COMPLETED
}
