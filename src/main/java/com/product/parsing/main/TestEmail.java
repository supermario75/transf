package com.product.parsing.main;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import javax.ws.rs.core.MediaType;

public class TestEmail
{

  public static void main(String[] args)
  {
    SendSimpleMessage();
  }

  public static ClientResponse SendSimpleMessage() {
    Client client = Client.create();
    client.addFilter(new HTTPBasicAuthFilter("api",
                                             "key-d792e8696aaa35c91384baff843aee13"));
    WebResource webResource =
        client.resource("https://api.mailgun.net/v3/sandbox3cb67ac1bc934765893f68b73c5cf868.mailgun.org/messages");
    MultivaluedMapImpl formData = new MultivaluedMapImpl();
    formData.add("from", "Mailgun Sandbox <postmaster@sandbox3cb67ac1bc934765893f68b73c5cf868.mailgun.org>");
    formData.add("to", "vestititrendy <vestititrendy@gmail.com>");
    formData.add("subject", "Hello vestititrendy");
    formData.add("text", "prova jersey");
    return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
        post(ClientResponse.class, formData);
  }
}
