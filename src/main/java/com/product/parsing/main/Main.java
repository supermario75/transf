package com.product.parsing.main;


import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Main
{

  public static void main(String[] args) throws XMLStreamException, IOException
  {
    final String categoryName = args[0];
    final Cobrand cobrand = Cobrand.valueOf(args[1]);

    Category category = Categories.byName(categoryName, cobrand);
    System.out.println("processing category");

    generateJsonAndSitemap(cobrand, category);

  }

  private static void generateJsonAndSitemap(Cobrand cobrand, Category category)
  {
    new Generator(cobrand).withCustomCategory(category).generateCustom(true,false,true,false, false);
  }

  private static void sleepSeconds(int seconds)
  {
    try
    {
      System.out.println("--------------sleeping " + seconds + " seconds");
      Thread.sleep(seconds * 1000);
      System.out.println("--------------awake!!! ");
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }
  }
























































}