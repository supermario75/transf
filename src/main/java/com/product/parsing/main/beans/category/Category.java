package com.product.parsing.main.beans.category;

import com.product.parsing.main.beans.Supplier;

import java.util.List;
import java.util.Objects;

public class Category
{
  private final String name;
  private final String friendlyName;
  private final List<String> parentTags;
  private final List<String> specialTags;
  private final List<Supplier> suppliers;
  private final boolean active;
  private final boolean includedInCarousel;
  private boolean allowedSearchTagsInDescription;
  private boolean includeInSitemap;
  private boolean shouldSort;
  private String genderType;

  public Category(String name,
                  String friendlyName,
                  String genderType,
                  List<String> parentTags,
                  List<String> specialTags,
                  List<Supplier> suppliers,
                  boolean active,
                  boolean includedInCarousel,
                  boolean allowedSearchTagsInDescription,
                  boolean includeInSitemap,
                  boolean shouldSort)
  {
    this.name = name;
    this.friendlyName = friendlyName;
    this.genderType = genderType;
    this.parentTags = parentTags;
    this.specialTags = specialTags;
    this.suppliers = suppliers;
    this.active = active;
    this.includedInCarousel = includedInCarousel;
    this.allowedSearchTagsInDescription = allowedSearchTagsInDescription;
    this.includeInSitemap = includeInSitemap;
    this.shouldSort = shouldSort;
  }

  public String getFriendlyNameLowercase()
  {
    return this.friendlyName.toLowerCase();
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    Category category = (Category) o;
    return Objects.equals(active, category.active) &&
        Objects.equals(includedInCarousel, category.includedInCarousel) &&
        Objects.equals(allowedSearchTagsInDescription, category.allowedSearchTagsInDescription) &&
        Objects.equals(name, category.name) &&
        Objects.equals(friendlyName, category.friendlyName) &&
        Objects.equals(parentTags, category.parentTags) &&
        Objects.equals(specialTags, category.specialTags) &&
        Objects.equals(suppliers, category.suppliers) &&
        Objects.equals(genderType, category.genderType) &&
        Objects.equals(shouldSort, category.shouldSort);
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(name,
                        friendlyName,
                        parentTags,
                        specialTags,
                        suppliers,
                        active,
                        includedInCarousel,
                        allowedSearchTagsInDescription,
                        genderType,
                        shouldSort);
  }

  @Override
  public String toString()
  {
    return "Category{" +
        "name='" + name + '\'' +
        ", friendlyName='" + friendlyName + '\'' +
        ", parentTags=" + parentTags +
        ", specialTags=" + specialTags +
        ", suppliers=" + suppliers +
        ", active=" + active +
        ", includedInCarousel=" + includedInCarousel +
        ", allowedSearchTagsInDescription=" + allowedSearchTagsInDescription +
        ", includeInSitemap=" + includeInSitemap +
        ", genderType='" + genderType + '\'' +
        ", shouldSort='" + shouldSort + '\'' +
        '}';
  }

  public String getName()
  {
    return name;
  }

  public String getFriendlyName()
  {
    return friendlyName;
  }

  public List<String> getParentTags()
  {
    return parentTags;
  }

  public List<String> getSpecialTags()
  {
    return specialTags;
  }

  public List<Supplier> getSuppliers()
  {
    return suppliers;
  }

  public boolean isActive()
  {
    return active;
  }

  public boolean isIncludedInCarousel()
  {
    return includedInCarousel;
  }

  public String getNameLowerCase()
  {
    return name.toLowerCase();
  }

  public boolean isAllowedSearchTagsInDescription()
  {
    return allowedSearchTagsInDescription;
  }

  public String getGenderType()
  {
    return genderType;
  }

  public boolean isIncludeInSitemap()
  {
    return includeInSitemap;
  }

  public boolean isShouldSort() {
    return shouldSort;
  }
}
