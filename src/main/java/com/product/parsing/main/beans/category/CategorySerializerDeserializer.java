package com.product.parsing.main.beans.category;

import com.google.gson.*;
import com.product.parsing.main.beans.Supplier;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategorySerializerDeserializer implements JsonSerializer<Category>,JsonDeserializer<Category>
{
  @Override
  public JsonElement serialize(Category category, Type type, JsonSerializationContext jsonSerializationContext)
  {
    final JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("name", category.getName());
    jsonObject.addProperty("genderType", category.getGenderType());
    jsonObject.addProperty("friendlyName", category.getFriendlyName());
    jsonObject.addProperty("parentTags", serializeArray(category.getParentTags()));
    jsonObject.addProperty("specialTags", serializeArray(category.getSpecialTags()));
    jsonObject.addProperty("suppliers", serializeArraySupplier(category.getSuppliers()));
    jsonObject.addProperty("active", category.isActive());
    jsonObject.addProperty("allowedSearchTagsInDescription", category.isAllowedSearchTagsInDescription());
    jsonObject.addProperty("includedInCarousel", category.isIncludedInCarousel());
    jsonObject.addProperty("shouldSort", category.isShouldSort());
    return jsonObject;
  }

  @Override
  public Category deserialize(JsonElement jsonElement,
                              Type type,
                              JsonDeserializationContext jsonDeserializationContext) throws JsonParseException
  {

    JsonObject jsonObject = (JsonObject) jsonElement;
    String name = jsonObject.get("name").getAsString();

    String friendlyName = jsonObject.get("friendlyName").getAsString();
    String genderType = jsonObject.get("genderType").getAsString();
    boolean allowedSearchTagsInDescription = jsonObject.get("allowedSearchTagsInDescription").getAsBoolean();
    List<String> parentTags = deserializeArray(jsonObject.get("parentTags").getAsString());
    List<String> specialTags = deserializeArray(jsonObject.get("specialTags").getAsString());
    List<Supplier> suppliers = deserializeArraySupplier(jsonObject.get("suppliers").getAsString());
    boolean active = jsonObject.get("active").getAsBoolean();
    boolean includedInCarousel = jsonObject.get("includedInCarousel").getAsBoolean();
    boolean includeInSitemap = booleanOrDefault("includeInSiteMap",true, jsonObject);
    boolean shouldSort = booleanOrDefault("shouldSort",false, jsonObject);

    return new Category(name,
                        friendlyName,
                        genderType,
                        parentTags,
                        specialTags,
                        suppliers,
                        active,
                        includedInCarousel,
                        allowedSearchTagsInDescription,
                        includeInSitemap,
                        shouldSort);
  }

  private boolean booleanOrDefault(String key, boolean defaultValue, JsonObject jsonObject)
  {
    JsonElement element = jsonObject.get(key);

    return element != null ? element.getAsBoolean() : defaultValue;
  }

  private List<Supplier> deserializeArraySupplier(String elements)
  {
    if (elements == null || "".equals(elements))
      return new ArrayList<>();

    List<Supplier> suppliers  = new ArrayList<>();
    final String[] split = elements.split("\\|");
    for(String s : split)
    {
      suppliers.add(Supplier.valueOf(s));
    }
    return suppliers;
  }

  private List<String> deserializeArray(String elements)
  {
    if (elements == null || "".equals(elements))
      return new ArrayList<>();

    return Arrays.asList(elements.split("\\|"));
  }

  private String serializeArray(List<String> list)
  {
    if (list == null || list.size() == 0)
      return "";

    StringBuffer sb = new StringBuffer();
    int count= 0;
    for(String elem : list)
    {
      if (count>0)
       sb.append("|");
      sb.append(elem);
      count++;
    }

    return sb.toString();
  }

  private String serializeArraySupplier(List<Supplier> suppliers)
  {
    if (suppliers == null || suppliers.size() == 0)
      return "";

    StringBuffer sb = new StringBuffer();
    int count= 0;
    for(Supplier supplier : suppliers)
    {
      if (count>0)
        sb.append("|");
      sb.append(supplier);
      count++;
    }

    return sb.toString();
  }


}
