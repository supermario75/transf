package com.product.parsing.main.beans;

import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.productFilter.DuplicateNameProductsFilter;
import com.product.parsing.main.productFilter.ProductsFilter;

public class CategorizedProducts
{
  private Products womenCategorized;
  private Products menCategorized;
  private Products childrenCategorized;
  private Category category;

  public CategorizedProducts()
  {
    this.menCategorized = new Products();
    this.womenCategorized = new Products();
    this.childrenCategorized = new Products();
  }

  public Products getWomenCategorized()
  {
    return womenCategorized;
  }

  public Products getMenCategorized()
  {
    return menCategorized;
  }

  public Products getChildrenCategorized()
  {
    return childrenCategorized;
  }

  public Category getCategory() { return category;  }

  public CategorizedProducts removeDuplicates(GenderProducts genderProducts, Category category)
  {
    ProductsFilter productsFilter = new DuplicateNameProductsFilter();

    womenCategorized = productsFilter.filter(genderProducts.getWomenProducts());
    menCategorized = productsFilter.filter(genderProducts.getMenProducts());
    childrenCategorized = productsFilter.filter(genderProducts.getChildrenProducts());
    this.category = category;
    return this;
  }

  public void add(CategorizedProducts partialCatProducts)
  {
    addIfNotEmpty(partialCatProducts.getWomenCategorized(), this.womenCategorized);
    addIfNotEmpty(partialCatProducts.getMenCategorized(), this.menCategorized);
    addIfNotEmpty(partialCatProducts.getChildrenCategorized(), this.childrenCategorized);
  }

  private void addIfNotEmpty(Products source, Products destination)
  {
    if (!source.isEmpty())
    destination.addAll(source);
  }

  public void setCategory(Category category)
  {
    this.category = category;
    final String categoryFriendlyNameLowercase = category.getFriendlyNameLowercase();

    for (Product product : womenCategorized)
      product.setCategory(categoryFriendlyNameLowercase);

    for (Product product : menCategorized)
      product.setCategory(categoryFriendlyNameLowercase);

    for (Product product : childrenCategorized)
      product.setCategory(categoryFriendlyNameLowercase);
  }

  public void setSupplier(String supplierName)
  {
    for (Product product : womenCategorized)
      product.setSupplier(supplierName);

    for (Product product : menCategorized)
      product.setSupplier(supplierName);

    for (Product product : childrenCategorized)
      product.setSupplier(supplierName);
  }
}
