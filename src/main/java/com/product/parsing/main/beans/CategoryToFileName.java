package com.product.parsing.main.beans;


import com.product.parsing.main.beans.category.Category;

public class CategoryToFileName
{

  public static String toFileName(Category category)
  {
    return (category.getName().contains("_")) ? category.getFriendlyNameLowercase()
                                                             : category.getNameLowerCase();
  }
}
