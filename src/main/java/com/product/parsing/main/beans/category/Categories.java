package com.product.parsing.main.beans.category;

import com.flexiblecatalog.site.common.Cobrand;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

public class Categories implements Iterable<Category>
{
  private static Gson gson = createGson();
  private List<Category> categories;

  private static Map<Cobrand,Categories> instanceMap = new HashMap();


  public static Categories getInstance(Cobrand cobrand)
  {
    Categories categories = instanceMap.get(cobrand);
    if (categories == null)
    {
      categories = new Categories(cobrand);
      instanceMap.put(cobrand,categories);
    }

    return instanceMap.get(cobrand);
  }



  private Categories(Cobrand cobrand)
  {
    this.categories = read(cobrand);
  }

  @Override
  public Iterator<Category> iterator()
  {
    return categories.iterator();
  }

  public List<Category> getList()
  {
    return categories;
  }

  public int size()
  {
    return categories.size();
  }

  private List<Category> read(Cobrand cobrand)
  {
    ReadFileFromClassPath readFileFromClassPath = new ReadFileFromClassPath();

    final String json = readFileFromClassPath.readToString(buildPath(cobrand));

    return fromJson(json);
  }

  private String buildPath(Cobrand cobrand)
  {
    return "cobrand/" + cobrand.getBasePath() + "/categories.json";
  }


  public String toJson(List<Category> categories)
  {

    StringBuffer sb = new StringBuffer("[");
    int count = 0;
    for(Category category : categories)
    {
      if (count > 0)
        sb.append("\n,");
      sb.append(gson.toJson(category));

      count++;
    }
    sb.append("]");
    final String s = sb.toString();
    return s;
  }

  private static Gson createGson()
  {
    return new GsonBuilder()
                      .registerTypeAdapter(Category.class, new CategorySerializerDeserializer())
                      .setPrettyPrinting()
                      .create();
  }

  private static Type newTypeToken()
  {
    return new TypeToken<ArrayList<Category>>()
    {
    }.getType();
  }

  public static List<Category> fromJson(String categoriesJson)
  {
    return gson.fromJson(categoriesJson,newTypeToken());
  }


  public List<Category> validCategories()
  {
    return categories.stream()
                     .filter(cat -> cat.isActive())
                     .collect(Collectors.toList());
  }

  public Category getCategoryByName(String name)
  {
    return categories.stream()
                     .filter(cat -> cat.getName().equals(name))
                     .findFirst().orElseThrow(() -> new IllegalArgumentException("Category " + name + " does not exists please create it"));
  }

  public static Category byName(String name, Cobrand cobrand)
  {
    return Categories.getInstance(cobrand).getCategoryByName( name);
  }

  public  List<Category> onlyWomancategories()
  {
    return categories.stream()
              .filter(cat -> cat.getGenderType().equalsIgnoreCase("woman"))
              .collect(Collectors.toList());
  }

  public  List<Category> onlyMenCategories()
  {
    return categories.stream()
                     .filter(cat -> cat.getGenderType().equalsIgnoreCase("man"))
                     .collect(Collectors.toList());
  }

  Optional<Category> findByFriendlyName(String friendlyName) {

      return categories.stream().filter(c -> c.getFriendlyName().equals(friendlyName))
                         .findFirst();
  }
}
