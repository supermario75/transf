package com.product.parsing.main.beans;

import com.flexiblecatalog.site.common.Gender;
import com.product.parsing.Products;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

class GenderProducts
{
  Logger logger = Logger.getLogger(GenderProducts.class);

  private Products allProducts;
  private Products womenProducts;
  private Products menProducts;
  private Products childrenProducts;

  public GenderProducts(Products allProducts)
  {
    this.allProducts = allProducts;
  }

  public Products getWomenProducts()
  {
    return womenProducts;
  }

  public Products getMenProducts()
  {
    return menProducts;
  }

  public Products getChildrenProducts()
  {
    return childrenProducts;
  }

  public GenderProducts splitByGender()
  {
    Map<Gender, Products> genderProductsMap = allProducts.divideByGender();
    womenProducts = genderProductsMap.get(Gender.WOMAN);
    menProducts = genderProductsMap.get(Gender.MAN);
    childrenProducts = genderProductsMap.get(Gender.CHILDREN);
    return this;
  }

  public GenderProducts splitByGenderWithoutChildren() {

    logger.warn("**********************");
    logger.warn(" splitByGenderWithoutChildren removing children products ");
    logger.warn("**********************");

    GenderProducts genderProducts = splitByGender();
    genderProducts.childrenProducts = new Products();
    return genderProducts;
  }
}
