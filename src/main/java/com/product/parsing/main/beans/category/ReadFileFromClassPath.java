package com.product.parsing.main.beans.category;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.InputStream;

public class ReadFileFromClassPath
{
  Logger logger = Logger.getLogger(ReadFileFromClassPath.class);

  public String readToString(String filename)
  {
    try
    {
      InputStream inputStream = this.getClass().getClassLoader()
                                    .getResourceAsStream(filename);
      return IOUtils.toString(inputStream);
    }
    catch (Exception e)
    {
      logger.error("error reading from file " + filename + " " + e.getMessage(),e);
      return null;
    }

  }
}
