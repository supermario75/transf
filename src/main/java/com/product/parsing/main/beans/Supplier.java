package com.product.parsing.main.beans;

import java.util.ArrayList;
import java.util.List;

public enum Supplier
{
  SOLARIS_SPORT(true),
  MADE_IN_DESIGN(false),
  GUESS(true),
//  ZALANDO(true),
  AMAZON(true),
  ASOS(true),
  MISSBIKINI(true),
//  YOOX(true),
  DOUGLAS(true),
  BATA(true),
  MAXCO(true),
  BOGGI(true),
  MANZARA_IT(true),
  SUPERDRY_IT(true),
  REDOUTE_IT(true),
  BOOHOO(true),
  ROBE_DI_KAPPA(true),

  ASOS_FR(true),
  GUESS_FR(true),
  BODEN_FR(true),
  YOOX_FR(true),
  MANDM_FR(true),
  DESSOUS_FR(true),
  CIRILLUS_FR(true),
  INSLAND_FR(true),
  FORZIERI_FR(true),
  TAILORSTORE_FR(true),
  FARFETCH_FR(true),
  TOPMAN_FR(true),
  SURFDOME_FR(true),
  WOOLOVER_FR(true),
  MARLIES_FR(true),
  KIABI_FR(true),
  REDOUTE_AUBAINES_FR(true),
  REDOUTE_FR(true),
  RALPHLAUREN_FR(true),
  DOROTY_FR(true),
  SUPERDRY_FR(true),
  IDEALO_FR(true),

  GUESS_ES(true),
  SARENZA_ES(true),
  SURFDOME_ES(true),
  YOOX_ES(true),
  PULLBEAR_ES(true)
  ;

  private boolean isActive;

  Supplier(boolean isActive)
  {
    this.isActive = isActive;
  }

  public static List<Supplier> activeSuppliers()
  {
    List<Supplier> activeSuppliers = new ArrayList<>();

    for (Supplier supplier : Supplier.values())
    {
      if (supplier.isActive)
        activeSuppliers.add(supplier);
    }
   return activeSuppliers;
  }

  public String formatterName()
  {
    return this.name().toLowerCase().replaceAll("_","");
  }
}
