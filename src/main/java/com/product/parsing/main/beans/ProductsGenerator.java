package com.product.parsing.main.beans;

import com.flexiblecatalog.site.common.Cobrand;
import com.flexiblecatalog.site.common.Gender;
import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.events.Event;
import com.product.parsing.main.events.EventType;
import com.product.parsing.main.events.SupplierEvent;
import com.product.parsing.main.listeners.ProductGenerationEventListener;
import com.product.parsing.writer.ProductsReader;
import com.product.parsing.writer.XMLToProductsReader;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.product.parsing.main.events.EventType.*;

public class ProductsGenerator
{
  Logger logger = Logger.getLogger(ProductsGenerator.class);
  List<ProductGenerationEventListener> categorizedProductsListeners = new ArrayList<>();


  private final ConfigurationParameter conf;
  private List<Category> categories;

  public ProductsGenerator(Cobrand cobrand, boolean generateFileJson, boolean generateCarousel, boolean createSitemap,
                           List<ProductGenerationEventListener> categorizedProductsListeners, boolean createDb)
  {
    conf = ConfigurationParameter.newDefaultConfiguration(cobrand,
                                                          generateFileJson,
                                                          generateCarousel,
                                                          createSitemap,
                                                          createDb);

    this.categorizedProductsListeners.addAll(categorizedProductsListeners);
    this.categories = Categories.getInstance(cobrand).validCategories();
  }

  public void generate()
  {
    for (Category category : categories)
    {
      CategorizedProducts categorizedProducts = generateProductForCategory(conf, category);
      fireEvents(CATEGORIZED_PRODUCT_CREATED,conf, categorizedProducts);
    }

    fireEvents(PRODUCTS_GENERATION_COMPLETED,conf);

  }

  public void setCategories(List<Category> categories)
  {
    this.categories = categories;
  }



  private CategorizedProducts generateProductForCategory(ConfigurationParameter conf,
                                                                Category category)
  {
    System.out.println("generating products for category " + category.getFriendlyName());

    CategorizedProducts categorizedProducts = new CategorizedProducts();
    for (Supplier supplier : category.getSuppliers())
    {
      logger.info("reading product for  supplier " + supplier);
      Products allProducts = randomize(readProductForSupplier(conf.getBaseInputPath(), supplier,category));
      logger.info("read " + allProducts.size() + " product for supplier " + supplier);

      GenderProducts genderProducts = new GenderProducts(allProducts).splitByGenderWithoutChildren();
      CategorizedProducts partialCatProducts = removeDuplicates(category, supplier, genderProducts);

      fireSupplierEvent(SUPPLIER_PRODUCT_CREATION, conf, category, supplier, partialCatProducts);

      categorizedProducts.add(partialCatProducts);
    }

    categorizedProducts.setCategory(category);

    return categorizedProducts;

  }

  private CategorizedProducts removeDuplicates(Category category, Supplier supplier, GenderProducts genderProducts)
  {
    CategorizedProducts partialCatProducts = new CategorizedProducts().removeDuplicates(genderProducts, category);
    partialCatProducts.setSupplier(supplier.name().toLowerCase());
    return partialCatProducts;
  }

  private Products randomize(Products products)
  {
    Collections.shuffle(products);
    return products;
  }


  private void fireEvents(EventType eventType,ConfigurationParameter conf)
  {
    fireEvents(eventType,conf,null);
  }

  private void fireEvents(EventType eventType,ConfigurationParameter conf, CategorizedProducts categorizedProducts)
  {
    for (ProductGenerationEventListener listener : categorizedProductsListeners)
    {
      listener.fireEvent(new Event(eventType,conf , categorizedProducts ));
    }
  }

  private void fireSupplierEvent(EventType supplierProductCreation, ConfigurationParameter conf, Category category,
                                 Supplier supplier,
                                 CategorizedProducts partialCatProducts)
  {
    for (ProductGenerationEventListener listener : categorizedProductsListeners)
    {
      Event event = new Event(supplierProductCreation, conf, partialCatProducts);
      listener.fireEvent(new SupplierEvent(supplier, category, event));
    }
  }

  private static Products readProductForSupplier(String baseInputPath, Supplier supplier, Category category)
  {
    String supplierFileName = buildSupplierFileName(baseInputPath, supplier.name());

    ProductsReader qualityGateProductsReader = new QualityGateProductsReader(new XMLToProductsReader());
    return qualityGateProductsReader.readAndFilterProducts(supplierFileName, category);
  }

  private static String buildSupplierFileName(String baseInputPath, String supplierName)
  {
    return baseInputPath + supplierName.toLowerCase() + ".xml";
  }

  public static List<Products> splitOnCategory(Category category , Products products)
  {
    return splitOnTags(category.getSpecialTags(), products);

  }

  public static List<Products> splitOnTags(List<String> specialTags, Products products)
  {
    Products matched = new Products();
    Products other  = new Products();
    for (Product product : products)
    {
      if (product.hasAnyTags(specialTags))
        matched.add(product);
      else
        other.add(product);
    }
    return Arrays.asList(matched, other);

  }

}
