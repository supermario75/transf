package com.product.parsing.main.beans;

import com.flexiblecatalog.site.common.Cobrand;

public class ConfigurationParameter
{
  private final String baseInputPath;
  private final String baseOutputPath;
  private String baseSitemapPath;
  private Cobrand cobrand;
  private final boolean generateFileJson;
  private boolean generateCarousel;
  private final ExclusionCriteria exclusionCriteria;
  private boolean createSitemap;
  private boolean createDb;

  public static ConfigurationParameter newDefaultConfiguration(Cobrand cobrand,
                                                               boolean generateFileJson,
                                                               boolean generateCarousel,
                                                               boolean createSitemap,
                                                               boolean createDb)
  {
    final String cobrandPath = cobrand.getBasePath() + "/";
    String baseInputPath = "/opt/xml/" + cobrandPath;
    String baseOutputPath = "/opt/catalog-persistence/src/main/resources/" + cobrandPath;
    String baseSitemapPath = "/opt/sitemaps/" + cobrandPath;

    ExclusionCriteria exclusionCriteria
            = new ExclusionCriteriaBuilder()
            .excludeTag("bambini")
            .excludeProductName("SPEEDO BIKINI MARE BIKINI BIANCO")
            .excludeProductName("AQUA SPHERE SQUARE DARK GRAY COSTUME GRIGIO SCURO")
            .excludeProductName("AQUA SPHERE APAX BLACK WHITE COSTUME")
            .build();

    ConfigurationParameter configurationParameter =
            new ConfigurationParameter(baseInputPath,
                                       baseOutputPath,
                                       baseSitemapPath,
                                       cobrand,
                                       generateFileJson,
                                       generateCarousel,
                                       createSitemap,
                                       exclusionCriteria,
                                       createDb);
    return configurationParameter;
  }

  public ConfigurationParameter(String baseInputPath,
                                String baseOutputPath,
                                String baseSitemapPath,
                                Cobrand cobrand,
                                boolean generateFileJson,
                                boolean generateCarousel,
                                boolean createSitemap,
                                ExclusionCriteria exclusionCriteria,
                                boolean createDb)
  {
    this.baseInputPath = baseInputPath;
    this.baseOutputPath = baseOutputPath;
    this.baseSitemapPath = baseSitemapPath;
    this.cobrand = cobrand;
    this.generateFileJson = generateFileJson;
    this.createDb = createDb;
    this.generateCarousel = generateCarousel;
    this.createSitemap = createSitemap;
    this.exclusionCriteria = exclusionCriteria;
  }

  public String getBaseInputPath()
  {
    return baseInputPath;
  }

  public String getBaseOutputPath()
  {
    return baseOutputPath;
  }


  public ExclusionCriteria getExclusionCriteria()
  {
    return exclusionCriteria;
  }

  public boolean isGenerateCarousel() { return generateCarousel; }

  public boolean isGenerateFileJson() { return generateFileJson; }

  public boolean isCreateSitemap() { return createSitemap; }

  public void setCreateSitemap(boolean createSitemap)
  {
    this.createSitemap = createSitemap;
  }

  public boolean isCreateDb()
  {
    return createDb;
  }

  public void setCreateDb(boolean createDb)
  {
    this.createDb = createDb;
  }

  public String getBaseSitemapPath()
  {
    return baseSitemapPath;
  }

  public Cobrand getCobrand()
  {
    return cobrand;
  }
}
