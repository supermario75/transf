package com.product.parsing.main.beans;

import com.product.parsing.Product;

import java.util.ArrayList;
import java.util.List;

public class ExclusionCriteria
{
  List<String> tags = new ArrayList<>();
  List<String> productNames = new ArrayList<>();

  public ExclusionCriteria(List<String> tags, List<String> productNames)
  {
    this.tags = tags;
    this.productNames = productNames;
  }

  public boolean isProductToExclude(Product product)
  {
    return isExcludedName(product.getName())
           || hasTagsToExclude(product);
  }

  private boolean hasTagsToExclude(Product product)
  {
    return product.hasAnyTags(getTags());
  }

  private boolean isExcludedName(String name)
  {
    return getProductNames().contains(name);
  }

  public List<String> getTags()
  {
    return tags;
  }

  public List<String> getProductNames()
  {
    return productNames;
  }
}
