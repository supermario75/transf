package com.product.parsing.main.beans;

import java.util.ArrayList;
import java.util.List;

public class ExclusionCriteriaBuilder
{
  List<String> tagsToExclude = new ArrayList<>();
  List<String> productNamesToExclude = new ArrayList<>();

  public ExclusionCriteriaBuilder excludeTag(String tag)
  {
    this.tagsToExclude.add(tag);
    return this;
  }

  public ExclusionCriteriaBuilder excludeProductName(String productName)
  {
    this.productNamesToExclude.add(productName);
    return this;
  }

  public ExclusionCriteria build()
  {
    return new ExclusionCriteria(tagsToExclude, productNamesToExclude);
  }
}