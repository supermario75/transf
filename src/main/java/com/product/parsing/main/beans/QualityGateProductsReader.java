package com.product.parsing.main.beans;

import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.writer.ProductsReader;
import org.apache.log4j.Logger;

import java.util.function.Predicate;

import static java.lang.String.format;

public class QualityGateProductsReader implements ProductsReader {

    Logger logger = Logger.getLogger(QualityGateProductsReader.class);

    private ProductsReader delegate;
    private static Predicate<Product> lowQuality = lowQualityPredicate();

    public QualityGateProductsReader(ProductsReader delegate) {
        this.delegate = delegate;
    }

    @Override
    public Products readAndFilterProducts(String inputFileName, Category category) {
        Products products = delegate.readAndFilterProducts(inputFileName, category);

        int sizeBefore = products.size();
        products.removeIf(lowQuality);
        int sizeAfter = products.size();

        if (sizeBefore > sizeAfter)
            logger.warn(format("removed %d products by QualityGateProductsReader for file %s and category %s", sizeBefore - sizeAfter, inputFileName, category));
        return products;
    }

    private static Predicate<Product> lowQualityPredicate()
    {
        Predicate<Product> shortDescription = new LogAndPredicate(product -> product.getDescription() == null || product.getDescription().length() < 20,"shortDescription");
        Predicate<Product> noPrice = new LogAndPredicate(product -> product.getPrice() == null,"noPrice");

        Predicate<Product> shortName = new LogAndPredicate(product -> product.getName().length() < 6,"shortname");


        return shortName.or(
                shortDescription).or(
                noPrice);

        }

    private static class LogAndPredicate implements Predicate<Product> {

        Logger logger = Logger.getLogger(LogAndPredicate.class);

        private Predicate<Product> delegate;
        private String name;
        private boolean loggingEnabled;

        public LogAndPredicate(Predicate<Product> delegate,String name) {
            this(delegate,name,false);
        }

        public LogAndPredicate(Predicate<Product> delegate,String name,boolean loggingEnabled) {
            this.delegate = delegate;
            this.name = name;
            this.loggingEnabled = loggingEnabled;
        }

        @Override
        public boolean test(Product product) {
            boolean result = this.delegate.test(product);
            if (loggingEnabled && result)
                logger.info(String.format("product %s from supplier %s removed by QualityGateProductsReader because %s", product.getName(), product.getSupplier(), name));
            return result;
        }
    }
}

