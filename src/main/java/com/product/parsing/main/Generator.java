package com.product.parsing.main;


import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.main.beans.ProductsGenerator;
import com.product.parsing.main.beans.Supplier;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.listeners.*;
import com.product.parsing.main.listeners.keywords.GenerateKeywordsListener;
import com.product.parsing.mongo.MongoDbListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Generator
{


  private List<Category> customCategories;
  private Cobrand cobrand;

  public Generator(Cobrand cobrand)
  {
    this.cobrand = cobrand;
  }

  private void generate(boolean generateFileJson,
                        boolean generateCarousel,
                        boolean createSitemap,
                        boolean createDb,
                        boolean generateKeywords)
  {

    List<ProductGenerationEventListener> listeners = new ArrayList<>();
    listeners.add(new LogProductCreationListener());
    listeners.add(newSortAndWriteListener());
    listeners.add(new MongoDbListener());
//    listeners.add(new CarouselGeneratorListener(Supplier.valueOf(cobrand.getSupplierForCarousel())));
    listeners.add(new SitemapGeneratorListener());
    listeners.add(new AsosProductGenerationEventListener());

    if (generateKeywords)
      listeners.add(new GenerateKeywordsListener());


    ProductsGenerator productsGenerator = new ProductsGenerator(cobrand,
                                                                generateFileJson,
                                                                generateCarousel,
                                                                createSitemap,
                                                                listeners,
                                                                createDb);

    if (customCategoriesDefined())
      productsGenerator.setCategories(customCategories);

    productsGenerator.generate();
  }

  private ComposedProductEventListener newSortAndWriteListener() {
    return new ComposedProductEventListener(new SortProductsListener(),
                                            new JsonFileWriterListener());
  }

  private Supplier getSupplierFilterByCobrand(Cobrand cobrand)
  {
    return Supplier.valueOf(cobrand.getSupplierForCarousel());
  }

  private boolean customCategoriesDefined()
  {
    return customCategories != null && customCategories.size() >0;
  }

  public Generator withSupplier(Supplier supplier)
  {
    List<Category> categoryForSupplier = new ArrayList<>();
    List<Category> categories = Categories.getInstance(cobrand).validCategories();
    for (Category category : categories)
    {
      if (category.getSuppliers().contains(supplier))
        categoryForSupplier.add(category);
    }
    this.customCategories = categoryForSupplier;
    return this;
  }

  public Generator withCustomCategories(List<Category> customCategories)
  {
    this.customCategories = customCategories;
    return this;
  }

  public Generator withCustomCategory(Category category)
  {
    this.customCategories = Arrays.asList(category);
    return this;
  }

  public void generateCarousel()
  {

    generate(false, true, false, false, false);
  }

  public void generateFileJson()
  {

    generate(true, false, false, false, false);

  }

  public void generateDatabase()
  {

    generate(false, false, false, true, false);

  }

  public void generateSitemap()
  {

    generate(false, false, true, false, false);

  }

  public void generateCustom(boolean generateFileJson,
                             boolean generateCarousel,
                             boolean createSitemap,
                             boolean createDb,
                             boolean generateKeywords)
  {
    generate(generateFileJson, generateCarousel, createSitemap, createDb, generateKeywords);

  }


  public void generateKeywords()
  {
    generate(false, false, false, false, true);
  }
}
