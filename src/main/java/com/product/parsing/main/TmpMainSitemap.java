package com.product.parsing.main;

import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;

public class TmpMainSitemap
{

  public static void main(String[] args)
  {

    Cobrand cobrand = Cobrand.VETEMENT_DE_TENDANCE;

    Category categorychand = Categories.byName("BOTTES", cobrand);
    new Generator(cobrand).withCustomCategory(categorychand).generateFileJson();
  }
}
