package com.product.parsing.main;


import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ManualGenerateCarousel
{

  public static void main(String[] args) throws XMLStreamException, IOException
  {

    System.out.println("processing category");

    generateCarouselVETEMENTS();

  // generateCarouselVT();
 //   generateCarouselROPA_DE_MODA();

  }

  private static void generateCarouselROPA_DE_MODA()
  {
    Cobrand cobrand = Cobrand.ROPA_DE_MODA;

    new Generator(cobrand).withCustomCategories(Arrays.asList(Categories.byName("CAMISAS", cobrand),
                                                              Categories.byName("PANTALON", cobrand)))
                          .generateCarousel();
  }

  private static void generateCarouselVETEMENTS()
  {
    Cobrand cobrand = Cobrand.VETEMENT_DE_TENDANCE;

    new Generator(cobrand).withCustomCategories(Arrays.asList(Categories.byName("DUFFEL_COAT", cobrand),
                                                              Categories.byName("VESTES_COUPE_SIMPLE", cobrand)))
                          .generateCarousel();
  }

  private static void generateCarouselVT()
  {
    Cobrand cobrand = Cobrand.VESTITI_TRENDY;

    List<Category> categories = Categories.getInstance(cobrand).getList();

    Category tshirts = Categories.byName("TSHIRTS", cobrand);
    Category eleganti = Categories.byName("ELEGANTI", cobrand);


    new Generator(cobrand).withCustomCategories(Arrays.asList(tshirts, eleganti))
                          .generateCarousel();
  }

}