package com.product.parsing.main.productFilter;

import com.product.parsing.Product;
import com.product.parsing.main.beans.category.Category;

import java.util.List;

public class ByCategoryProductMatcher
{
  private Category category;


  public ByCategoryProductMatcher(Category category)
  {
    this.category = category;
  }

  public boolean applyTo(Product product)
  {
    boolean toAdd =false;
    List<String> categoryTags = category.getSpecialTags();
    List<String> parentTags = category.getParentTags();

    if ( ( productContainAllParentTags(product, parentTags) &&
           anyTagFoundInTagsOrInDescription(product, categoryTags)
         )
       )
      toAdd = true;
    return toAdd;
  }

private boolean productContainAllParentTags(Product product, List<String> parentTags)
{
  boolean found = true;
  for (String parentTag : parentTags)
  {
    found = product.hasTag(parentTag);
    if (!found)
      break;
  }
  return found;
}
private boolean anyTagFoundInTagsOrInDescription(Product product, List<String> categoryTags) {
  return product.hasAnyTags(categoryTags) || ( category.isAllowedSearchTagsInDescription() &&
                                          descriptionContainsAnyTag(product.getDescription(), categoryTags));
}
private boolean descriptionContainsAnyTag(String description, List<String> categoryTags)
{
  return containsAnyString(description, categoryTags);
}
private boolean containsAnyString(String s, List<String> specialTags)
{
  if (s == null)
    return false;

  String sLowerCase = s.toLowerCase();
  for (String specialTag : specialTags)
  {
    String tagLowerCase = specialTag.toLowerCase();
    if (sLowerCase.contains(tagLowerCase))
      return true;
  }
  return false;
}
}
