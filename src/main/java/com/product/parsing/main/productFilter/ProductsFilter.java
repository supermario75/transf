package com.product.parsing.main.productFilter;

import com.product.parsing.Products;

public interface ProductsFilter
{
  Products filter(Products products);
}
