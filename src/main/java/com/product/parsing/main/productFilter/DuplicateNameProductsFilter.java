package com.product.parsing.main.productFilter;

import com.product.parsing.Product;
import com.product.parsing.Products;

import java.util.HashMap;

public class DuplicateNameProductsFilter implements ProductsFilter
{

  @Override
  public Products filter(Products products)
  {
    int ignoredProduxts = 0;
    HashMap<String,String> names = new HashMap<>();
    Products filtered = new Products();
    for (Product product : products)
    {
      String nameLowercase = product.getName().toLowerCase();
      if (names.get(nameLowercase) == null)
      {
        names.put(nameLowercase,nameLowercase);
        filtered.add(product);
      }
      else
      {
        ignoredProduxts++;
      }

    }
    if (ignoredProduxts > 0)
      System.out.println(ignoredProduxts + " product ignored because duplicated");
    return filtered;
  }
}
