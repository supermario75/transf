package com.product.parsing.main.listeners;

import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.events.Event;
import com.product.parsing.main.events.EventType;
import org.apache.log4j.Logger;

public class SortProductsListener implements ProductGenerationEventListener {

    Logger logger = Logger.getLogger(SortProductsListener.class);
    @Override
    public void fireEvent(Event event) {

        if (event.getEventType() == EventType.CATEGORIZED_PRODUCT_CREATED)
        {
            Category category = event.getProducts().getCategory();
            if (category.isShouldSort())
            {
                logger.debug("---> sorting product for category " + category.getFriendlyName());

                event.getProducts().getMenCategorized().sortByQuality();
                event.getProducts().getWomenCategorized().sortByQuality();
                event.getProducts().getChildrenCategorized().sortByQuality();
            }

        }
    }
}
