package com.product.parsing.main.listeners;

import com.product.parsing.main.beans.Supplier;
import com.product.parsing.main.events.Event;
import com.product.parsing.main.events.EventType;
import com.product.parsing.main.events.SupplierEvent;
import org.apache.log4j.Logger;

public class AsosProductGenerationEventListener implements ProductGenerationEventListener
{
    Logger logger = Logger.getLogger(AsosProductGenerationEventListener.class);

    @Override
    public void fireEvent(Event event) {

        if (event.hasType(EventType.SUPPLIER_PRODUCT_CREATION) && ((SupplierEvent) event).isEventFor(Supplier.ASOS) )
        {
            logger.info("aaa tring to fill fillShippingHandlingCosts " + ((SupplierEvent) event).getSupplier());
            new AsosEnricher().enrich(event.getProducts());
        }
    }


}
