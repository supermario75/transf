package com.product.parsing.main.listeners;

import com.product.parsing.main.beans.CategorizedProducts;
import com.product.parsing.main.beans.CategoryToFileName;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.events.Event;
import com.product.parsing.main.events.EventType;
import com.product.parsing.writer.ProductFileWriter;
import org.apache.log4j.Logger;

public class JsonFileWriterListener implements ProductGenerationEventListener
{
  Logger logger = Logger.getLogger(JsonFileWriterListener.class);

  @Override
  public void fireEvent(Event event)
  {
    if (!event.getConf().isGenerateFileJson())
      return;


    if (event.getEventType() == EventType.CATEGORIZED_PRODUCT_CREATED)
    {
      final CategorizedProducts categorizedProducts = event.getProducts();
      final Category category = categorizedProducts.getCategory();
      writeCategorizedProductsToFile(category,event.getConf().getBaseOutputPath(),categorizedProducts);
      logger.debug("---> generated products for category " + category.getFriendlyName());
    }
  }

  private void writeCategorizedProductsToFile( Category category,
                                                     String baseOutputPath, CategorizedProducts categorizedProducts)
  {

      ProductFileWriter.write(categorizedProducts.getWomenCategorized(), newFileName(baseOutputPath, category, "donna_"));
      ProductFileWriter.write(categorizedProducts.getMenCategorized(), newFileName(baseOutputPath, category, "uomo_"));
      ProductFileWriter.write(categorizedProducts.getChildrenCategorized(), newFileName(baseOutputPath, category, "bambino_"));

  }

  private static String newFileName(String baseOutputPath, Category category, String menOrWoman)
  {
    String partialFileName = CategoryToFileName.toFileName(category);
    return baseOutputPath +
        menOrWoman +
        partialFileName + ".json";
  }

}
