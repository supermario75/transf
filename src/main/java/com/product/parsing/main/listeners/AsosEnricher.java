package com.product.parsing.main.listeners;

import com.product.parsing.Product;
import com.product.parsing.main.beans.CategorizedProducts;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.function.Consumer;

public class AsosEnricher {

    Logger logger = Logger.getLogger(AsosEnricher.class);
    public void enrich(CategorizedProducts products)
    {
        enrich(products, product -> enrichWithDeliveryExpenses(product));
        enrich(products, product -> enrichFrom(product));
    }

    private void enrich(CategorizedProducts products, Consumer<Product> consumer)
    {

        products.getMenCategorized().stream().forEach(consumer);
        products.getWomenCategorized().stream().forEach(consumer);
        products.getChildrenCategorized().stream().forEach(consumer);

    }

    private Product enrichWithDeliveryExpenses(Product product) {
        if (product.getShippingHandlingCost() == null)
            product.setShippingHandlingCost(priceGreaterThan(product, "24.99") ? "0" : "4.95");

        return product;

    }

    private boolean priceGreaterThan(Product product, String minValue) {
        BigDecimal price = new BigDecimal(product.getPrice());
        return price.compareTo(new BigDecimal(minValue)) > 0;
    }


    public void enrichFrom(Product product) {

        boolean isEmpty = product.getAdditionalImages().isEmpty();
        if (!isEmpty) {
            String firstAdditionalImage = product.getAdditionalImages().iterator().next();
            if (endsWith(firstAdditionalImage, ".jpg")){

                String baseUrl = removeLastTwoSlashes(firstAdditionalImage);

                product.getAdditionalImages().add(baseUrl + "/image2xxl.jpg");
                product.getAdditionalImages().add(baseUrl + "/image3xxl.jpg");
                product.getAdditionalImages().add(baseUrl + "/image1xxl.jpg");

                product.setImageUrl(baseUrl + "/image4xxl.jpg");
                product.setLargeImageUrl(baseUrl + "/image4xxl.jpg");
            }
        }
    }

    private boolean endsWith(String largeImageUrl, String suffix) {
        return largeImageUrl != null && largeImageUrl.endsWith(suffix);
    }

    private String removeLastTwoSlashes(String largeImageUrl) {
        String substring = largeImageUrl.substring(0, largeImageUrl.lastIndexOf("/"));
        return largeImageUrl.substring(0, substring.lastIndexOf("/"));
    }


}
