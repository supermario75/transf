package com.product.parsing.main.listeners.keywords;

import com.flexiblecatalog.site.common.Cobrand;
import com.flexiblecatalog.site.common.tag.TagAttributes;
import com.flexiblecatalog.site.common.tag.TagKeywords;
import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.Generator;
import com.product.parsing.main.beans.CategorizedProducts;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.writer.FiltUtil;

class GenerateKeywords
{
  GenerateKeywords(int numberOfKeywords)
  {
    this.numberOfKeywords = numberOfKeywords;
  }

  public void generateFrom(CategorizedProducts products, String categoryName)
  {
    TagAttributes.loadFromPath("/opt/stark-basin-3452/src/main/resources/tagAttributes.json");

    TagKeywords manKeywords   = TagKeywords.from(allWords(products.getMenCategorized()), numberOfKeywords);
    TagKeywords womanKeywords = TagKeywords.from(allWords(products.getWomenCategorized()), numberOfKeywords);

    TagAttributes.replaceKeywordForCategory(categoryName.toLowerCase(),manKeywords,womanKeywords);

    String json = TagAttributes.json();

    FiltUtil.writeStringToFile(json,"/opt/stark-basin-3452/src/main/resources/tagAttributes_mod.json");
  }

  private String[] allWords(Products products)
  {
    StringBuilder fullDesc = new StringBuilder();
    for(Product product : products)
    {
      fullDesc.append(product.getDescription()).append(" ");
    }

    return fullDesc.toString().split(" ");
  }


  public static void main(String[] args)
  {
   /* GenerateKeywords generateKeywords = new GenerateKeywords();
    generateKeywords.generateFrom(null,"eleganti");*/
    Cobrand cobrand = Cobrand.VESTITI_TRENDY;
    Category eleganti = Categories.byName("ABBIGLIAMENTO_SPORTIVO", cobrand);

    new Generator(cobrand).withCustomCategory(eleganti).generateKeywords();
  }

  private int numberOfKeywords;
}
