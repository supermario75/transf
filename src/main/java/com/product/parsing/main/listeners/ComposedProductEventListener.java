package com.product.parsing.main.listeners;

import com.product.parsing.main.events.Event;

public class ComposedProductEventListener implements ProductGenerationEventListener {

    private final ProductGenerationEventListener l1;
    private final ProductGenerationEventListener l2;

    public ComposedProductEventListener(ProductGenerationEventListener l1,
                                        ProductGenerationEventListener l2) {
        this.l1 = l1;
        this.l2 = l2;
    }

    @Override
    public void fireEvent(Event event) {

        l1.fireEvent(event);
        l2.fireEvent(event);
    }
}
