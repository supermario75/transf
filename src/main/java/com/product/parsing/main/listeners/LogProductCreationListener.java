package com.product.parsing.main.listeners;

import com.product.parsing.main.events.Event;

public class LogProductCreationListener implements ProductGenerationEventListener
{
  @Override
  public void fireEvent(Event event)
  {
    System.out.println("LogProductCreationListener ---------------------------> evento " + event.getEventType() );
  }


}
