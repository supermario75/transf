package com.product.parsing.main.listeners;

import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.main.beans.CategorizedProducts;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.events.Event;
import com.product.parsing.writer.FiltUtil;
import com.sitemap.XmlValidator;
import com.sitemap.sitemapbuilder.ProductsSitemapFactory;
import com.sitemap.sitemapbuilder.Sitemap;
import com.sitemap.sitemapbuilder.SitemapBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class SitemapGeneratorListener implements ProductGenerationEventListener {
    private DateFormat w3dtfFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");



    @Override
    public void fireEvent(Event event) {
        if (!event.getConf().isCreateSitemap())
            return;

        switch (event.getEventType()) {
            case CATEGORIZED_PRODUCT_CREATED:

                Category category = event.getProducts().getCategory();
                if (!category.isIncludeInSitemap())
                {
                    System.out.println("category " + category.getName() + "excluded from sitemap");
                    break;
                }

                System.out.println("[SitemapGeneratorListener]  updating sitemap with product for category " + category.getFriendlyName());

                List<Sitemap> sitemaps = new ProductsSitemapFactory().createSitemaps(event);
                for(Sitemap sitemap : sitemaps)
                  writeToFileAndValidate(event, sitemap);

                break;
            case PRODUCTS_GENERATION_COMPLETED:

                String content = new ProductsSitemapFactory().createSitemapIndex(event);
                FiltUtil.writeStringToFile(content, event.getConf().getBaseSitemapPath() + "sitemap_index.xml");

                break;
        }

    }

    private void writeToFileAndValidate(Event event, Sitemap sitemap) {
        String fileName = event.getConf().getBaseSitemapPath() + sitemap.getName();
        try
        {
            tryWriteSitemap(fileName, sitemap.getText());
        }
        catch (Exception e)
        {
            System.err.println("error writing and validating sitemap " + fileName  + " " + e.getMessage() );
            e.printStackTrace();
        }
    }

    private Map<Category, CategorizedProducts> byCatebogyProductMap(Event event) {
        Map<Category,CategorizedProducts> productsMap = new HashMap<>();
        productsMap.put(event.getProducts().getCategory(),
                        event.getProducts());
        return productsMap;
    }



    private void tryWriteSitemap(String fileName, String text)
    {
        FiltUtil.writeStringToFile(text, fileName);
        System.out.println("[SitemapGeneratorListener]  wrote sitemap to " + fileName);
        new XmlValidator().doValidation(fileName);
    }

    private void generateSitemapIndex(String fileName, Cobrand cobrand)
    {
        List<Category> categories = Categories.getInstance(cobrand).getList();
        SitemapBuilder sitemapBuilder = new SitemapBuilder(cobrand.getSiteUrl());
        String sitemapIndexString = sitemapBuilder.generateSitemapIndex(cobrand,w3dtfFormat.format(new Date()), categories);
        FiltUtil.writeStringToFile(sitemapIndexString, fileName);
    }


}
