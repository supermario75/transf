package com.product.parsing.main.listeners;

import com.carousel.CarouselGenerator;
import com.carousel.CarouselStrategy;
import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.CategorizedProducts;
import com.product.parsing.main.beans.Supplier;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.events.Event;
import com.product.parsing.main.events.EventType;
import com.product.parsing.main.events.SupplierEvent;
import com.product.parsing.writer.FiltUtil;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static com.product.parsing.main.events.EventType.PRODUCTS_GENERATION_COMPLETED;
import static com.product.parsing.main.events.EventType.SUPPLIER_PRODUCT_CREATION;

public class CarouselGeneratorListener implements ProductGenerationEventListener
{
  CarouselStrategy carouselManStrategy = new CarouselStrategy();
  CarouselStrategy carouselWomanStrategy = new CarouselStrategy();
  private Supplier supplierFilter;


  public CarouselGeneratorListener(Supplier supplierFilter)
  {
    this.supplierFilter = supplierFilter;
  }

  @Override
  public void fireEvent(Event event)
  {
    if (!event.getConf().isGenerateCarousel())
      return;

    EventType eventType = event.getEventType();

    if (eventType == SUPPLIER_PRODUCT_CREATION)
    {
      updateCarouselProducts((SupplierEvent) event, supplierFilter);
    }

    if (eventType == PRODUCTS_GENERATION_COMPLETED)
    {
      Cobrand cobrand = event.getConf().getCobrand();
      generateCarouselString(cobrand);
    }
  }

  private void generateCarouselString(Cobrand cobrand)
  {
    Products womanProducts = carouselWomanStrategy.getProducts();
    Products manProducts = carouselManStrategy.getProducts();

    CarouselGenerator carouselGenerator = new CarouselGenerator();

    String basePath = cobrand.getBasePath();
    String language = cobrand.getLanguage();

    long currentTimeMillis = System.currentTimeMillis();
    String carouselWoman = carouselGenerator.createCarousel("Woman",basePath, womanProducts,currentTimeMillis, language);
    String carouselMan = carouselGenerator.createCarousel("Man",basePath, manProducts, currentTimeMillis, language);

    String carouselString = carouselWoman + "\n\n\n" + carouselMan;


    String fileName = "/opt/stark-basin-3452/src/main/webapp/js/" + basePath  + "/carousel.js";
    FiltUtil.writeStringToFile(carouselString, fileName);
    System.out.println("wrote file " + fileName);

    downLoadImages("woman",basePath, womanProducts);
    downLoadImages("man",basePath, manProducts);

    updateCarouselVersionIn("/opt/stark-basin-3452/src/main/webapp/WEB-INF/template/default/footer.jsp",
                            currentTimeMillis);

    System.out.println("downloaded images" + fileName);


  }

  private void updateCarouselVersionIn(String fileName, long currentTimeMillis)
  {
    String content = readStringFromFile(fileName);
    String carouselIncludeVersion = "/carousel.js?version=";

    if (!content.contains(carouselIncludeVersion))
      throw new RuntimeException(fileName + " does not contain string " + carouselIncludeVersion);

    int start = content.indexOf(carouselIncludeVersion) + carouselIncludeVersion.length();
    int end = content.indexOf("\"",start);

    StringBuilder sb = new StringBuilder();
    sb.append(content.substring(0,start))
      .append(String.valueOf(currentTimeMillis))
      .append(content.substring(end, content.length()));

    String newContent = sb.toString();


    FiltUtil.writeStringToFile(newContent, fileName);

  }

  private String readStringFromFile(String fileName)
  {
    try
    {
      return tryReadStringFromFile(new File(fileName));
    }
    catch(IOException e)
    {
     throw new RuntimeException(e);
    }
  }

  public static String tryReadStringFromFile(File file)
      throws IOException
  {
    int len;
    char[] chr = new char[4096];
    final StringBuffer buffer = new StringBuffer();
    final FileReader reader = new FileReader(file);
    try {
      while ((len = reader.read(chr)) > 0) {
        buffer.append(chr, 0, len);
      }
    } finally {
      reader.close();
    }
    return buffer.toString();
  }

  private String downLoadImages(String gender, String basePath, Products products)
  {
    int index = 0;
    String wgetStr = "";
    for (Product product : products)
    {
      System.out.println("nome prodotto  " + product.getName() + " slide_" + index);
      String singleCommand ="wget -O /opt/stark-basin-3452/src/main/webapp/carousel/"+ basePath  +"/slide_" + gender.toLowerCase() + "_" + index + ".jpg" +
                            " " + product.getLargeImageUrl();
      execCommand(singleCommand);
      wgetStr += "\n" + singleCommand;

      index++;
    }
    return wgetStr;
  }

  private void execCommand(String singleCommand)
  {
    try
    {
      System.out.println("executing command " + singleCommand);
      Process process = Runtime.getRuntime().exec(singleCommand);
      process.waitFor();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private void updateCarouselProducts(SupplierEvent supplierEvent, Supplier supplierFilter)
  {

    Category category = supplierEvent.getCategory();
    Supplier supplier = supplierEvent.getSupplier();
    CategorizedProducts partialCatProducts = supplierEvent.getProducts();

    if (supplier == supplierFilter)
    {
      carouselManStrategy.filterProducts(category, partialCatProducts.getMenCategorized(), "uomo");
      carouselWomanStrategy.filterProducts(category, partialCatProducts.getWomenCategorized(), "donna");
      carouselWomanStrategy.filterProducts(category, partialCatProducts.getChildrenCategorized(), "bambino");
    }
  }

}
