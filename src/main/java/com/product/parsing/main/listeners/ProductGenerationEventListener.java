package com.product.parsing.main.listeners;

import com.product.parsing.main.events.Event;

public interface ProductGenerationEventListener
{
  public void fireEvent(Event event);
}
