package com.product.parsing.main.listeners.keywords;

import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.events.Event;
import com.product.parsing.main.events.EventType;
import com.product.parsing.main.listeners.ProductGenerationEventListener;

public class GenerateKeywordsListener implements ProductGenerationEventListener
{
  public static final int NUMBER_OF_KEYWORDS = 50;
  private final GenerateKeywords generateKeywords = new GenerateKeywords(NUMBER_OF_KEYWORDS);

  @Override
  public void fireEvent(Event event)
  {
    if (event.getEventType() == EventType.CATEGORIZED_PRODUCT_CREATED) {
      Category category = event.getProducts().getCategory();
      System.out.println("GenerateKeywordsListener ---------------------------> evento " + event.getEventType() );

      generateKeywords.generateFrom(event.getProducts(), category.getFriendlyName());
    }

  }



}
