package com.product.parsing.writer;

import java.io.FileWriter;
import java.io.IOException;

public class FiltUtil
{
  public static void writeStringToFile(String content, String fileName) {
    FileWriter fileWriter = null;
    try {

      fileWriter = new FileWriter(fileName);
      fileWriter.write(content);
      fileWriter.close();
    } catch (IOException ex) {
      System.err.println(ex.getMessage());
      throw new RuntimeException(ex);
    } finally {
      try {
        fileWriter.close();
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    }
  }
}
