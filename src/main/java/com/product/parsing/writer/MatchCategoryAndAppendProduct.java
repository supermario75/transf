package com.product.parsing.writer;

import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.eventlistener.singleproduct.SingleProductListener;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.productFilter.ByCategoryProductMatcher;

public class MatchCategoryAndAppendProduct implements SingleProductListener
{
  Products products = new Products();
  private final ByCategoryProductMatcher byCategoryProductMatcher;

  public MatchCategoryAndAppendProduct(Category category)
  {
    byCategoryProductMatcher = new ByCategoryProductMatcher(category);
  }

  @Override
  public void onProduct(Product product)
  {
    if (byCategoryProductMatcher.applyTo(product)) products.add(product);
  }


  public Products getProducts()
  {
    return products;
  }
}
