package com.product.parsing.writer;

import com.product.parsing.ProductEventListener;
import com.product.parsing.Products;
import com.product.parsing.ZanoxProductParser;
import com.product.parsing.eventlistener.DefaultEventListener;
import com.product.parsing.main.beans.category.Category;

import java.util.Arrays;

public class XMLToProductsReader implements ProductsReader {
  @Override
  public  Products readAndFilterProducts(String inputFileName, Category category)
  {
    MatchCategoryAndAppendProduct matchCategoryAndAppendProduct = new MatchCategoryAndAppendProduct(category);

    ZanoxProductParser zanoxProductParser = createParser(matchCategoryAndAppendProduct);
    zanoxProductParser.parseProducts(inputFileName);

    Products products = matchCategoryAndAppendProduct.getProducts();
    System.out.println("read " + products.size() + " products");
    return products;
  }

  private static ZanoxProductParser createParser(MatchCategoryAndAppendProduct matchCategoryAndAppendProduct)
  {
    ProductEventListener productEventListener = new DefaultEventListener(Arrays.asList(matchCategoryAndAppendProduct));

    return new ZanoxProductParser(productEventListener);
  }

}
