package com.product.parsing.writer;

import com.product.parsing.Products;
import com.product.parsing.main.beans.category.Category;

public interface ProductsReader {
    Products readAndFilterProducts(String inputFileName, Category category);
}
