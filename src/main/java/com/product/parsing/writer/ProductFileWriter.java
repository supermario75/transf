package com.product.parsing.writer;

import com.product.parsing.Products;
import org.apache.log4j.Logger;

public class ProductFileWriter
{
  public static Logger logger = Logger.getLogger(ProductFileWriter.class);

  public static void write(Products products, String outputFileName)
  {
    if (products.size() == 0)
    {
      logger.warn("no products in input for " + outputFileName);
      return;
    }

    FiltUtil.writeStringToFile(products.toString(), outputFileName);
    logger.warn("wrote product on file " + outputFileName);
  }

}
