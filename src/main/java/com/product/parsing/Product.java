package com.product.parsing;

import com.flexiblecatalog.site.common.Gender;
import com.product.parsing.seo.SeoFormatter;
import com.product.parsing.seo.SeoFormatterImpl;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Product
{
  Logger logger = Logger.getLogger(Product.class);

  String imageUrl ="";
  String url ="";
  String namePrevious="";
  String name="";
  String manufacturer="";
  String seoManufacturer="";
  Set<String> tags = new HashSet<>();
  Set<String> additionalImages = new HashSet<>();
  String price="";


  String oldPrice="";
  private String largeImageUrl ="";
  private SeoFormatter seoFormatter;
  private SquareBraceReplacer squareBraceReplacer;

  private String seoNamePrevious;
  private String seoName;
  private String description;
  private String shippingHandlingCost;
  private String deliveryTime;
  private String color;
  private String seoColor;
  private String category;
  private String supplier;
  private String gender;
  private String size;
  private String additionalFeatures;
  private String material;
  private String pixel;
  private String sku;


  public Product()
  {
    this.seoFormatter = SeoFormatterImpl.getInstance();
    this.squareBraceReplacer = SquareBraceReplacer.getInstance();
  }

  public Product(SeoFormatter seoFormatter)
  {
    this.seoFormatter = seoFormatter;
  }

  public String getImageUrl()
  {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl)
  {
    this.imageUrl = imageUrl;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = squareBraceReplacer.replaceBraces(url);
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getNamePrevious() {
    return namePrevious;
  }

  public void setNamePrevious(String namePrevious) {
    this.namePrevious = namePrevious;
  }

  public String getPrice()
  {
    return price;
  }

  public void setPrice(String price)
  {
    this.price = price;
  }

  public String getManufacturer()
  {
    return manufacturer;
  }

  public void setManufacturer(String manufacturer)
  {
    this.manufacturer = manufacturer;
    this.seoManufacturer = seoFormatter.nameToSeoName(manufacturer);
  }

  public String getLargeImageUrl() { return largeImageUrl; }

  public void setLargeImageUrl(String largeImageUrl) { this.largeImageUrl = largeImageUrl; }

  public Set<String> getTags()
  {
    return tags;
  }

  @Override
  public String toString()
  {
    return new StringBuilder()
            .append(" { ")
            .append("\n").append(formatField("name", name)).append(" ,")
            .append("\n").append(formatField("namePrevious", namePrevious)).append(" ,")
            .append("\n").append(formatField("category", category)).append(" ,")
            .append("\n").append(formatField("gender", gender)).append(" ,")
            .append("\n").append(formatField("supplier", supplier)).append(" ,")
            .append("\n").append(formatField("seoName", seoName)).append(" ,")
            .append("\n").append(formatField("seoNamePrevious", seoNamePrevious)).append(" ,")
            .append("\n" + formatTags()).append(" ,")
            .append("\n" + formatAdditionalImages()).append(" ,")
            .append("\n").append(formatNumericField("price", price)).append(" ,")
            .append("\n").append(formatNumericField("oldPrice", oldPrice)).append(" ,")
            .append("\n").append(formatField("imageUrl", imageUrl)).append(" ,")
            .append("\n").append(formatField("largeImageUrl", largeImageUrl)).append(" ,")
            .append("\n").append(formatField("pixel", pixel)).append(" ,")
            .append("\n").append(formatField("sku", sku)).append(" ,")
            .append("\n").append(formatField("description", description)).append(" ,")
            .append("\n").append(formatField("color", color)).append(" ,")
            .append("\n").append(formatField("size", size)).append(" ,")
            .append("\n").append(formatField("additionalFeatures", additionalFeatures)).append(" ,")
            .append("\n").append(formatField("additionalFeaturesMap", additionalFeaturesMap)).append(" ,")
            .append("\n").append(formatField("material", material)).append(" ,")
            .append("\n").append(formatField("seoColor", seoColor)).append(" ,")
            .append("\n").append(formatField("shippingHandlingCost", shippingHandlingCost)).append(" ,")
            .append("\n").append(formatField("deliveryTime", deliveryTime)).append(" ,")
            .append("\n").append(formatField("manufacturer", manufacturer)).append(" ,")
            .append("\n").append(formatField("seoManufacturer", seoManufacturer)).append(" ,")
            .append("\n").append(formatField("url", url))
            .append("\n").append(" } ")
            .toString();
  }



  public String formatField(String name ,String inputValue)
  {
    String value = inputValue;
    if (inputValue == null)
    {
      //System.out.println("no value for key " + name);
      value="";
    }

    StringBuilder sb = new StringBuilder()
      .append("  \"").append(name).append("\": ")
      .append("  \"").append(value.trim()).append("\"");
    return sb.toString();
  }

  public String formatNumericField(String name ,String value)
  {
    if (value == null)
      return "";

    StringBuilder sb = new StringBuilder()
            .append("  \"").append(name).append("\": ")
            .append(value.trim());
    return sb.toString();
  }

  public String formatTags()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("  \"tagList\" : [");

    int counter = 0;
    for (String tagValue : tags)
    {
      if (counter > 0)
        sb.append(",");
      sb.append("{ \"tagName\" : \"").append(tagValue).append("\" }");
      counter ++;
    }

    sb.append(" ]");
    return sb.toString();
  }

  private String formatAdditionalImages()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("  \"additionalImages\" : [");

    int counter = 0;
    for (String tagValue : additionalImages)
    {
      if (counter > 0)
        sb.append(",");
      sb.append("\"").append(tagValue).append("\"");
      counter ++;
    }

    sb.append(" ]");
    return sb.toString();
  }

  public void setIfNotPresentImageUrl(String tagContent)
  {
    if (isEmpty(imageUrl))
      setImageUrl(tagContent);
  }

  public void setIfNotPresentPrice(String tagContent)
  {
    if (isEmpty(price))
      setPrice(tagContent);
  }

  public void setIfNotPresentOldPrice(String tagContent)
  {
    if (isEmpty(oldPrice))
      setOldPrice(tagContent);
  }

  public void setIfNotPresentUrl(String tagContent)
  {
    if (isEmpty(url))
      setUrl(tagContent);
  }

  public void setIfNotPresentName(String tagContent)
  {
    if (isEmpty(name))
    {
      String cleanedTagContent = InvalidCharsRemover.removeFrom(tagContent);
      this.name = cleanedTagContent;
      setSeoName(seoFormatter.nameToSeoName(cleanedTagContent));
    }
  }

  public void setIfNotPresentNamePrevious(String tagContent)
  {
    if (isEmpty(namePrevious))
    {
      String cleanedTagContent = InvalidCharsRemover.removeFrom(tagContent);
      this.namePrevious = cleanedTagContent;
      setSeoNamePrevious(seoFormatter.nameToSeoName(cleanedTagContent));
    }
  }

  public void addTag(String tag)
  {
    tags.add(tag);
  }

  public void addAdditionalImage(String imageUrl)
  {
    additionalImages.add(imageUrl);
  }


  public String getOldPrice()
  {
    return oldPrice;
  }

  public void setOldPrice(String oldPrice)
  {
    this.oldPrice = oldPrice;
  }

  public void setIfNotManufacturer(String content)
  {
    if (isEmpty(manufacturer))
      setManufacturer(content);
  }

  public void setIfNotPresentLargeImageUrl(String content)
  {
    if (isEmpty(largeImageUrl))
      setLargeImageUrl(content);
  }

  public void setIfNotPresentDescription(String content)
  {
    if (isEmpty(description))
      setDescription(content);
  }

  public void setIfNotPresentShippingHandlingCost(String content)
  {
    if (isEmpty(shippingHandlingCost))
      setShippingHandlingCost(content);
  }

  public void setIfNotPresentDeliveryTime(String content)
  {
    if (isEmpty(deliveryTime))
      setDeliveryTime(content);
  }

  private boolean isEmpty(String str)
  {
    if (str == null || str.equals(""))  return true;
    return false;
  }

  public boolean hasAnyTags(List<String> specialTags)
  {
    for (String specialTag : specialTags)
    {
      if (hasTag(specialTag))
        return true;
    }
    return false;
  }

  public boolean hasTag(String specialTag) {
    for (String tag : tags)
    {
      if (tag.toLowerCase().contains(specialTag.toLowerCase()))
      {
        return true;
      }
    }
    return false;
  }

  public void setSeoName(String seoName)
  {
    this.seoName = seoName;
  }

  public String getSeoName()
  {
    return seoName;
  }

  public String getSeoNamePrevious() {
    return seoNamePrevious;
  }

  public void setSeoNamePrevious(String seoNamePrevious) {
    this.seoNamePrevious = seoNamePrevious;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public void setShippingHandlingCost(String shippingHandlingCost)
  {
    this.shippingHandlingCost = shippingHandlingCost;
  }

  public String getShippingHandlingCost()
  {
    return shippingHandlingCost;
  }

  public void setDeliveryTime(String deliveryTime)
  {
    this.deliveryTime = deliveryTime;
  }

  public String getDeliveryTime()
  {
    return deliveryTime;
  }

  public void setColor(String color)
  {
    this.color = color;
    this.seoColor = seoFormatter.nameToSeoName(color);
  }

  public String getColor() { return color; }

  public String getSeoManufacturer() { return seoManufacturer; }

  public void setSeoManufacturer(String seoManufacturer) { this.seoManufacturer = seoManufacturer; }

  public String getSeoColor()   { return seoColor; }

  public void setSeoColor(String seoColor) { this.seoColor = seoColor; }

  public void setCategory(String categoryString)
  {
    this.category = categoryString;
  }

  public String getCategory()
  {
    return category;
  }

  public void setSupplier(String supplier)
  {
    this.supplier = supplier;
  }

  public String getSupplier()
  {
    return supplier;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public String getGender()
  {
    return gender;
  }

  public void setSize(String size)
  {

    this.size = size;
  }

  public String getSize()
  {
    return size;
  }

  public void setAdditionalFeatures(String additionalFeatures)
  {
    this.additionalFeatures = additionalFeatures;
  }

  public String getAdditionalFeatures()
  {
    return additionalFeatures;
  }

  public void setMaterial(String material)
  {
    this.material = material;
  }

  public String getMaterial()
  {
    return material;
  }

  public boolean hasGender(Gender genderen)
  {
    return this.gender != null && gender.equals(genderen.name());
  }

  public boolean hasAnyGender() {
    return this.gender != null && !this.gender.isEmpty();
  }

  public void setGenderFromTagOrDefault(Gender defaultGender)
  {
    Set<String> tags = this.tags;
    Gender foundGender = null;
    for (String tag : tags) {
      foundGender = Gender.from(tag);
      if ( foundGender != null)
        break;
    }
    this.gender = foundGender != null ? foundGender.name()
                                      : defaultGender.name();
  }

  public void setPixel(String pixel) {
    this.pixel = pixel;
  }

  public String getPixel() {
    return pixel;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public String getSku() {
    return sku;
  }

  private class FieldValueDoesNotExists extends RuntimeException
  {
    public FieldValueDoesNotExists(String key)
    {
      super("value null for key = " + key);
    }
  }

  public void setAdditionalFeaturesMap(String additionalFeaturesMap)
  {
    this.additionalFeaturesMap = additionalFeaturesMap;
  }

  private String additionalFeaturesMap;

  public String getAdditionalFeaturesMap()
  {
    return additionalFeaturesMap;
  }

  public Set<String> getAdditionalImages() {

    return additionalImages;
  }
}
