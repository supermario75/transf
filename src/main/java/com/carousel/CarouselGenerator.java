package com.carousel;

import com.flexiblecatalog.site.common.Translations;
import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.taggedProducts.TaggedProduct;

public class CarouselGenerator
{


  public String createCarousel(String gender, String basePath, Products products, long currentTime, String language)
  {
    String str = "app.controller('Carousel" + gender+  "Ctrl',function ($scope) {\n"+
                 "    $scope.myInterval = 5000;\n" +
                 "    var slides = $scope.slides = [];\n" +
                 "    var version = " + currentTime + ";\n"+
                 "    $scope.addSlide = function() {\n" +
                 "        var newWidth = slides.length;\n" +
                 "        slides.push({\n" +
                 "            image: '/carousel/" + basePath + "/slide_" + gender.toLowerCase()+  "_' + newWidth + '.jpg?version=' + version,\n";

/*    str += "            image: [\n";
    str = fillWithImageUrl(str, products);
    str +="\n                 ][slides.length % " + products.size()+  "] ,\n";*/

    str += "            text: [\n";
    str = fillWithProductName(str, products);
    str +="\n                 ][slides.length % " + products.size()+  "] ,\n";

    str += "            price: [\n";
    str = fillWithProductPrice(str, products);
    str +="\n                 ][slides.length % " + products.size()+  "] ,\n";

    str += "            url: [\n";
    str = fillWithProductDetailUrl(str, products, language);
    str +="\n                 ][slides.length % " + products.size()+  "] \n";

    str +=       "        });\n" +
                 "    };\n" +
                 "    for (var i=0; i<" + products.size()+  "; i++) {\n" +
                 "        $scope.addSlide();\n" +
                 "    }\n" +
                 "});";


    return str;
  }



  private void execCommand(String singleCommand)
  {
    try
    {
      System.out.println("executing command " + singleCommand);
      Process process = Runtime.getRuntime().exec(singleCommand);
      process.waitFor();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private  String fillWithImageUrl(String str, Products products)
  {
    int counter = 0;
    for (Product product : products)
    {
      str+= "\t\t\t\t'" + product.getLargeImageUrl() + "'";
      if (counter != products.size()-1)
        str += ",\n";
      counter++;
    }
    return str;
  }

  private  String fillWithProductName(String str, Products products)
  {
    int counter = 0;
    for (Product product : products)
    {
     str+= "\t\t\t\t'" + product.getName() + "'";
      if (counter != products.size()-1)
        str += ",\n";
     counter++;
    }
    return str;
  }

  private  String fillWithProductPrice(String str, Products products)
  {
    int counter = 0;
    for (Product product : products)
    {
      str+= "\t\t\t\t'" + product.getPrice() + "'";
      if (counter != products.size()-1)
        str += ",\n";
      counter++;
    }
    return str;
  }



  private  String fillWithProductDetailUrl(String str, Products products, String language)
  {

    int counter = 0;
    for (Product product : products)
    {
      TaggedProduct taggedProduct = (TaggedProduct) product;
      String url = buildUrl(taggedProduct, language);
      str+= "\t\t\t\t'" +url + "'";
      if (counter != products.size()-1)
        str += ",\n";
      counter++;
    }
    return str;
  }

  private String buildUrl(TaggedProduct taggedProduct, String language)
  {
    String translatedDetail = Translations.translate("dettaglio", language);
    String baseUrl ="gender/tag/" + translatedDetail + "/seoname";
    return replaceParameters(taggedProduct, baseUrl, language);

  }

  private String replaceParameters(TaggedProduct taggedProduct, String baseUrl, String language)
  {
    String translatedGender = Translations.translate(taggedProduct.getGender(), language);
    String url = baseUrl.replaceFirst("gender",translatedGender);
    url = url.replaceFirst("tag",taggedProduct.getTag());
    url = url.replaceFirst("seoname",taggedProduct.getSeoName());
    return url;
  }
}
