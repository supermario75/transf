package com.carousel;

import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.taggedProducts.TaggedProduct;

import java.util.Random;


public class CarouselStrategy
{
  Products products = new Products();
  private int numProductForCategory = 4;

  private static Product getRandomElement(Products products)
  {
    Random myRandomizer = new Random();
    return products.get(myRandomizer.nextInt(products.size()));
  }

  public void filterProducts(Category category, Products inputProducts,String gender)
  {
    if (!inputProducts.isEmpty() && category.isIncludedInCarousel())
    {

      if ("COSTUMI".equals(category.getName()) )
      {
        filterSwimsuits(inputProducts);
      }
      else
      {
        for (int i = 0; i < numProductForCategory; i++)
        {
          products.add(newTaggedProduct(category,
                                        getRandomElement(inputProducts),
                                        gender)
                      );
        }
      }

    }

  }

  private TaggedProduct newTaggedProduct(Category category, Product randomElement,String gender)
  {
    String tag = category.getFriendlyName().toLowerCase();
    return new TaggedProduct(randomElement, tag,gender);
  }

  private void filterSwimsuits(Products inputProducts)
  {
    int added = 0;
    for (Product inputProduct : inputProducts)
    {
      if (added >= numProductForCategory)
        break;


      if (productNameContains(inputProduct, "fichissima") ||
          productNameContains(inputProduct, "maestrale") )
      {
        products.add(inputProduct);
        added ++;
      }
    }
  }

  private boolean productNameContains(Product inputProduct, String fichissima)
  {
    return inputProduct.getName().toLowerCase().contains(fichissima);
  }

  public Products getProducts()
  {
    return products;
  }
}
