package com.sitemap;

import org.apache.log4j.xml.SAXErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileReader;
import java.io.IOException;

public class XmlValidator
{

  public void doValidation(String fileName)
  {
    try
    {
      tryDoValidation(fileName);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private void tryDoValidation(String fileName) throws ParserConfigurationException, SAXException, IOException
  {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    factory.setValidating(true);
    factory.setNamespaceAware(true);

    SAXParser parser = factory.newSAXParser();

    XMLReader reader = parser.getXMLReader();
    reader.setErrorHandler(new SAXErrorHandler());
    reader.parse(new InputSource(new FileReader(fileName)));
  }
}
