package com.sitemap.sitemapbuilder;

import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.Product;
import com.product.parsing.Products;
import com.product.parsing.main.beans.CategorizedProducts;
import com.product.parsing.main.beans.category.Categories;
import com.product.parsing.main.beans.category.Category;
import com.product.parsing.main.events.Event;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.flexiblecatalog.site.common.Translations.translate;


public class ProductsSitemapFactory
{

    private DateFormat w3dtfFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public List<Sitemap> createSitemaps(Event event) {
        Map<Category, CategorizedProducts> productsMap = byCategoryProductMap(event);


        List<Sitemap> sitemaps = new ArrayList<>();
        sitemaps.add(firstSitemap(event.getConf().getCobrand()));
        for (Category category : productsMap.keySet())
        {
            System.out.println("building sitemap for category " + category.getName());
            SitemapBuilder sitemapBuilder = new SitemapBuilder(event.getConf().getCobrand().getSiteUrl());

            addProductDetailUrlToBuilder(productsMap.get(category), sitemapBuilder, event.getConf().getCobrand());

            List<String> texts = sitemapBuilder.build();
            int count = 0;
            for (String text : texts)
            {
                String cntStr = (count == 0) ? "" : "" + count;
                String name = "sitemap_" + category.getFriendlyName().toLowerCase() + cntStr + ".xml";
                sitemaps.add(newSitemap(name, text));
                count++;
            }

        }
        return sitemaps;
    }

    public String createSitemapIndex(Event event)
    {
        Cobrand cobrand = event.getConf().getCobrand();
        List<Category> categories = Categories.getInstance(cobrand).getList();
        SitemapBuilder sitemapBuilder = new SitemapBuilder(cobrand.getSiteUrl());
        return sitemapBuilder.generateSitemapIndex(cobrand,w3dtfFormat.format(new Date()), categories);
    }


    private Map<Category, CategorizedProducts> byCategoryProductMap(Event event) {
        Map<Category,CategorizedProducts> productsMap = new HashMap<>();
        productsMap.put(event.getProducts().getCategory(),
                event.getProducts());
        return productsMap;
    }

    private Sitemap firstSitemap(Cobrand cobrand)
    {
        final String siteUrl = cobrand.getSiteUrl();
        SitemapBuilder sitemapBuilder = new SitemapBuilder(siteUrl);

        String language = cobrand.getLanguage();
        String vestiti = translate("vestiti", language);
        String uomo = translate("uomo", language);
        String donna = translate("donna", language);

        sitemapBuilder
                .addUrl(siteUrl + "/" + vestiti + "/" + uomo)
                .addUrl(siteUrl + "/" + vestiti + "/" + donna)
        ;

        final Categories categories = Categories.getInstance(cobrand);

        List<Category> onlywomanCat = categories.onlyWomancategories();
        List<Category> onlymanCat = categories.onlyMenCategories();

        for (Category category : categories.validCategories())
        {
            String[] genders = {uomo, donna};
            for (String gender : genders)
            {
                if (uomo.equals(gender) && onlywomanCat.contains(category))
                    continue;

                if (donna.equals(gender) && onlymanCat.contains(category))
                    continue;

                sitemapBuilder.addUrl(siteUrl + "/" + vestiti + "/" + gender + "/" + category.getFriendlyName().toLowerCase());
            }
        }

        Sitemap sitemap = new Sitemap();
        sitemap.setName("main.xml");
        sitemap.setText(sitemapBuilder.build().get(0));

        return sitemap;
    }

    private Sitemap newSitemap(String name, String text) {
        Sitemap sitemap = new Sitemap();

        sitemap.setName(name);
        sitemap.setText(text);
        return sitemap;
    }

    private void addProductDetailUrlToBuilder(CategorizedProducts categorizedProducts, SitemapBuilder sitemapBuilder,Cobrand cobrand) {
        String tag = categoryToTag(categorizedProducts.getCategory());

        List<String> womenProdUrls = extractWomenUrls(categorizedProducts, tag,cobrand);
        for (String url : womenProdUrls)
            sitemapBuilder.addUrl(url);

        List<String> menProdUrls = extractMenUrls(categorizedProducts, tag,cobrand);
        for (String url : menProdUrls)
            sitemapBuilder.addUrl(url);

        List<String> childrenUrls = extractChildrenUrls(categorizedProducts, tag,cobrand);
        for (String url : childrenUrls)
            sitemapBuilder.addUrl(url);
    }

    private List<String> buildUrls(String tag, Products womenCategorized, String gender,Cobrand cobrand) {
        List<String> urls = new ArrayList<>();
        String base = cobrand.getSiteUrl() + "/" + translate("vestiti", cobrand.getLanguage()) +"/";

        String baseWithGender = new StringBuilder(base).append(gender).toString();
        for (Product product : womenCategorized) {
            urls.add(buildDetailUrl(baseWithGender, tag, product,cobrand));
        }
        return urls;
    }


    private String buildDetailUrl(String baseWithGender, String tag, Product product,Cobrand cobrand) {
        return new StringBuilder(baseWithGender)
                .append("/").append(tag)
                .append("/"  + translate("dettaglio", cobrand.getLanguage()))
                .append("/")
                .append(product.getSeoName()).toString();
    }

    private String categoryToTag(Category category) {
        return category.getFriendlyName().toLowerCase();
    }

    private List<String> extractMenUrls(CategorizedProducts categorizedProducts, String tag,Cobrand cobrand) {
        Products menCategorized = categorizedProducts.getMenCategorized();
        return buildUrls(tag, menCategorized, translate("uomo", cobrand.getLanguage()),cobrand);
    }

    private List<String> extractWomenUrls(CategorizedProducts categorizedProducts, String tag,Cobrand cobrand) {
        Products womenCategorized = categorizedProducts.getWomenCategorized();
        return buildUrls(tag, womenCategorized, translate("donna", cobrand.getLanguage()),cobrand);
    }

    private List<String> extractChildrenUrls(CategorizedProducts categorizedProducts, String tag,Cobrand cobrand) {
        Products childrenCategorized = categorizedProducts.getChildrenCategorized();
        return buildUrls(tag, childrenCategorized, translate("bambino", cobrand.getLanguage()),cobrand);
    }



}
