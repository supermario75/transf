package com.sitemap.sitemapbuilder;

import com.flexiblecatalog.site.common.Cobrand;
import com.product.parsing.main.beans.category.Category;
import com.sitemap.sitemapbuilder.beans.SitemapUrlBuilder;
import com.sitemap.sitemapbuilder.beans.Url;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class SitemapBuilder
{
  List<Url> urls = new ArrayList<>();
  public static final int MAX_SITEMAP_SIZE = 49900;
  private String siteUrl;

  public SitemapBuilder(String siteUrl)
  {
    this.siteUrl = siteUrl;
  }


  public SitemapBuilder addUrl(String name)
  {
    return addUrl(name, Url.ChangeFreq.WEEKLY);
  }

  public SitemapBuilder addUrl(String name, Url.ChangeFreq changeFreq)
  {
    Url url = new SitemapUrlBuilder(name)
            .withChangefreq(changeFreq)
            .withPriority(Url.Priority.NORMAL)
            .withtLastMod(new Date())
            .build();
    this.urls.add(url);

    return this;
  }

  public List<String> build()
  {
    int totalUrlsNumber = urls.size();

    int startIndex = 0;
    int toIndex = Math.min(totalUrlsNumber, MAX_SITEMAP_SIZE);

    List<String> sitemaps = new ArrayList<>();

    while(startIndex < totalUrlsNumber)
    {
      List<Url> reducedUrls = urls.subList(startIndex, toIndex);

      sitemaps.add(replaceUrlsInTemplate(reducedUrls));

      startIndex = toIndex;
      toIndex = Math.min(totalUrlsNumber, startIndex+ MAX_SITEMAP_SIZE);
    }

    return sitemaps;

  }

  private String replaceUrlsInTemplate(List<Url> reducedUrls)
  {
    String sitemapTemplatefile = "/opt/transf/src/main/java/com/sitemap/sitemapbuilder/sitemap_template.xml";

    String templateString = fileToString(sitemapTemplatefile);

    templateString = templateString.replace("urlPlaceHolder", urlsToString(reducedUrls));
    return templateString;
  }

  private String urlsToString(List<Url> urls1)
  {
    StringBuilder sb = new StringBuilder();
    for (Url url : urls1)
    {
      sb.append(url.toString());
    }
    return sb.toString();
  }

  private String fileToString(String sitemapTemplatefile)
  {
    String result;
    try
    {
      Path path = Paths.get(sitemapTemplatefile);
      result = tryFileToString(path);
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
    return result;
  }

  private String tryFileToString(Path path) throws IOException
  {
    return new String(Files.readAllBytes(path));
  }

  public String generateSitemapIndex(Cobrand cobrand,String lastModDate, List<Category> categories)
  {
    String sitemapIndexTemplatefile = "/opt/transf/src/main/java/com/sitemap/sitemapbuilder/sitemap_index_template.xml";
    String sitemapFragmentfile = "/opt/transf/src/main/java/com/sitemap/sitemapbuilder/sitemap_fragment.xml";

    String templateString = fileToString(sitemapIndexTemplatefile);
    String templateFragmentString = fileToString(sitemapFragmentfile);

    String relativeSitemapPath = cobrand.getConfiguration().getRelativeSitemapPath();

    StringBuilder content = new StringBuilder();
    content.append(buildFragment(concat(siteUrl,relativeSitemapPath,"/mysm.xml"), lastModDate, templateFragmentString));

    final List<Category> validCategories = categories.stream().filter(cat -> cat.isActive()).collect(Collectors.toList());
    for (Category category : validCategories)
      content.append(buildFragment(concat(siteUrl,relativeSitemapPath,"/sitemap_",category.getFriendlyName().toLowerCase(),".xml.gz"),
                                   lastModDate,
                                   templateFragmentString));

    String replacement = content.toString();
    String sitemapIndexString = templateString.replace("content", replacement);

    return sitemapIndexString;
  }

  private String concat(String ... strs)
  {
    StringBuilder sb  =new StringBuilder();
    for(String str : strs)
      sb.append(str);

    return sb.toString();
  }

  private String buildFragment(String replacement,
                               String lastModDate,
                               String templateFragmentString)
  {
    String fragment = templateFragmentString;
    fragment= fragment.replace("sitemapUrl", replacement);
    fragment= fragment.replace("sitemapDate", lastModDate);
    return fragment;
  }
}
