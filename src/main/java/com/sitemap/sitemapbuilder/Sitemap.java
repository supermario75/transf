package com.sitemap.sitemapbuilder;

/**
 * Created by user on 04/01/15.
 */
public class Sitemap
{
    String name;
    String text;

    public Sitemap() {
    }

    public Sitemap(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
