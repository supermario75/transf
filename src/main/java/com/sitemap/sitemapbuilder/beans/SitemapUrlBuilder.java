package com.sitemap.sitemapbuilder.beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SitemapUrlBuilder
{
  private String loc;
  private Date lastMod;
  private Url.ChangeFreq changefreq;
  private Url.Priority priority;
  private List<String> urlImages = new ArrayList<String>();
  private SimpleDateFormat fmt;

  public SitemapUrlBuilder(String loc)
  {
    this.loc = loc;
  }

  public SitemapUrlBuilder withtLastMod(Date lastMod)
  {
    this.lastMod = lastMod;
    return this;
  }

  public SitemapUrlBuilder withChangefreq(Url.ChangeFreq changefreq)
  {
    this.changefreq = changefreq;
    return this;
  }

  public SitemapUrlBuilder withPriority(Url.Priority priority)
  {
    this.priority = priority;
    return this;
  }

  public SitemapUrlBuilder withUrlImages(List<String> urlImages)
  {
    this.urlImages = urlImages;
    return this;
  }

  public SitemapUrlBuilder setFmt(SimpleDateFormat fmt)
  {
    this.fmt = fmt;
    return this;
  }

  public Url build()
  {
    return new Url(loc, lastMod, changefreq, priority, urlImages);
  }
}