package com.sitemap.sitemapbuilder.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Url
{

  String loc;
  Date lastMod;
  ChangeFreq changefreq;
  Priority priority;
  List<String> urlImages;

  private DateFormat w3dtfFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");


  public Url(String loc, Date lastMod, ChangeFreq changefreq, Priority priority,
             List<String> urlImages)
  {
    this.loc = loc;
    this.lastMod = lastMod;
    this.changefreq = changefreq;
    this.priority = priority;
    this.urlImages = urlImages;
  }

  public Date getLastMod()
  {
    return lastMod;
  }

  @Override
  public String toString()
  {
    return "\n  " + "<url>" +
           wrapWithTags("loc", loc) +
           wrapWithTags("lastmod", w3dtfFormat.format(lastMod)) +
           wrapWithTags("changefreq", changefreq.getText()) +
           wrapWithTags("priority", priority.getText()) +
           urlImagesToString(urlImages) +
           "\n  " + "</url>";
  }


  private String urlImagesToString(List<String> urlImages)
  {

    if  (isEmpty(urlImages))   return "";

    String toReturn ="";
    for (String urlImage : urlImages)
    {
      toReturn +=
             "\n<image:image>" +
               wrapWithTags("image:loc",urlImage) +
             "\n</image:image>";
    }
    return toReturn;
  }

  private boolean isEmpty(List<String> urlImages)
  {
    return urlImages == null ||
         urlImages.size() == 0;
  }

  private String wrapWithTags(String tagName,String value)
  {
    return (value != null) ? "\n\t" + openTag(tagName) + value + closeTag(tagName)
                           : "";
  }

  private String closeTag(String tagName)
  {
    return "</" + tagName + ">";
  }

  private String openTag(String tagName)
  {
    return "<" + tagName + ">";
  }

  public enum ChangeFreq
  {
    ALWAYS,
    HOURLY,
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY,
    NEVER;

    public String getText()
    {
      return this.name().toLowerCase();
    }

  }

  public enum Priority
  {
    LOW("0.0"),
    NORMAL("0.5"),
    HIGH("0.8"),
    MAX("1.0");
    private String text;

    Priority(String text)
    {
      this.text = text;
    }

    public String getText()
    {
      return text;
    }
  }
}
